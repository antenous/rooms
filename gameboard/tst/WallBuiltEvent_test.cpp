// Copyright (c) 2021 Antero Nousiainen

#include <gameboard/WallBuiltEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::gameboard
{
namespace
{
TEST(WallBuiltEventTest, SubscribeEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        WallBuiltEvent::subscribe([&subscriberInvoked](const WallPosition&) {
            subscriberInvoked = true;
        });

    std::invoke(WallBuiltEvent{}, WallPosition{});
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::gameboard
