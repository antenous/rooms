// Copyright (c) 2020 Antero Nousiainen

#include "Wall.hpp"
#include "Room.hpp"
#include <gmock/gmock.h>

namespace rooms::gameboard
{
namespace
{
using namespace testing;

class WallTest : public Test
{
protected:
    Room room{Rect{}};
    Wall wall{room};
};

TEST_F(WallTest, IsBuiltByDefault)
{
    EXPECT_TRUE(wall.built());
}

TEST_F(WallTest, CanThrowAndCatchWallAlreadyBuilt)
{
    const auto throwWallAlreadyBuilt = [] {
        throw Wall::AlreadyBuilt{};
    };
    EXPECT_THAT(
        throwWallAlreadyBuilt, ThrowsMessage<GameboardError>(StrEq("wall already built")));
}

TEST_F(WallTest, ThrowWhenBuildingAlreadyBuiltWall)
{
    EXPECT_THROW(wall.build(), Wall::AlreadyBuilt);
}

TEST_F(WallTest, IsNotBuiltWhenLinkedToSecondRoom)
{
    wall.link(room);
    EXPECT_FALSE(wall.built());
}

TEST_F(WallTest, IsBuildableWhenLinkedWithSecondRoom)
{
    wall.link(room);
    wall.build();
    EXPECT_TRUE(wall.built());
}
} // namespace
} // namespace rooms::gameboard
