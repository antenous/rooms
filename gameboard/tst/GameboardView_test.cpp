// Copyright (c) 2022 Antero Nousiainen

#include <gameboard/GameboardView.hpp>
#include <gameboard/Gameboard.hpp>
#include <common/Utility.hpp>
#include <fmt/format.h>
#include <gmock/gmock.h>
#include <ranges>

inline namespace
{
// NOTE: A bit of an ugly workaround since Point is actually an alias
using namespace rooms;
void PrintTo(Point point, std::ostream* os)
{
    *os << fmt::format("({}, {})", point.x(), point.y());
}
} // namespace

namespace rooms
{
inline namespace
{
// LCOV_EXCL_START
void PrintTo(const WallPosition& wall, std::ostream* os)
{
    *os << fmt::format(
        "{{{}, {}}}", testing::PrintToString(wall.position), to_underlying(wall.side));
}
// LCOV_EXCL_STOP
} // namespace
} // namespace rooms

namespace rooms::gameboard
{
namespace
{
using namespace testing;

class GameboardViewTest : public Test
{
protected:
    void buildWall(Point position, Side side)
    {
        gameboard.build({position, side});
    }

    Dimensions dimensions{5, 5, 3};
    GameboardView gameboardView;
    Gameboard gameboard{dimensions};
};

TEST_F(GameboardViewTest, IsNoncopyable)
{
    EXPECT_FALSE(std::is_copy_constructible_v<GameboardView>);
    EXPECT_FALSE(std::is_copy_assignable_v<GameboardView>);
}

TEST_F(GameboardViewTest, IsMovable)
{
    EXPECT_TRUE(std::is_move_constructible_v<GameboardView>);
    EXPECT_TRUE(std::is_move_assignable_v<GameboardView>);
}

TEST_F(GameboardViewTest, ReturnLayoutWhenPositionIsKnown)
{
    EXPECT_EQ((Rect{0, 0, dimensions.width, dimensions.height}), gameboardView.layout({0, 0}));
    EXPECT_EQ(
        (Rect{dimensions.width, dimensions.height, dimensions.width, dimensions.height}),
        gameboardView.layout({1, 1}));
    EXPECT_EQ(
        (Rect{dimensions.width, 0, dimensions.width, dimensions.height}),
        gameboardView.layout({1, 0}));
    EXPECT_EQ(
        (Rect{0, dimensions.height, dimensions.width, dimensions.height}),
        gameboardView.layout({0, 1}));
}

TEST_F(GameboardViewTest, NearestUnbuiltWall)
{
    /*
     +---+---+---+
     |   |   |   |
     +---+...+---+
     |   : x :   |
     +---+...+---+
     |   |   |   |
     +---+---+---+
     */
    // clang-format off
    const GameboardView::Walls walls{
        {{0, 0}, Side::bottom},
        {{0, 0}, Side::right},
        {{2, 0}, Side::left},
        {{2, 0}, Side::bottom},
        {{0, 2}, Side::top},
        {{0, 2}, Side::right},
        {{2, 2}, Side::top},
        {{2, 2}, Side::left}};
    // clang-format on
    std::ranges::for_each(walls, [this](auto wall) {
        buildWall(wall.position, wall.side);
    });

    const GameboardView::Walls expected{
        {{1, 1}, Side::left},
        {{1, 1}, Side::top},
        {{1, 1}, Side::bottom},
        {{1, 1}, Side::right}};

    for (auto e : expected)
    {
        const auto wall =
            gameboardView.nearestUnbuiltWall(dimensions.width + 1, dimensions.height + 2);
        ASSERT_EQ(e, wall);
        buildWall(wall->position, wall->side);
    }
}

TEST_F(GameboardViewTest, ReturnNulloptWhenRoomHasNoUnbuiltWalls)
{
    /*
     +---+---+---+
     | x |   :   |
     +---+.......+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    // clang-format off
    const GameboardView::Walls walls{
        {{0, 0}, Side::bottom},
        {{0, 0}, Side::right}};
    // clang-format on
    std::ranges::for_each(walls, [this](auto wall) {
        buildWall(wall.position, wall.side);
    });

    EXPECT_FALSE(gameboardView.nearestUnbuiltWall(1, 2));
}

TEST_F(GameboardViewTest, ReturnNulloptWhenCoordinatesNotInGameboard)
{
    /*
     +---+---+---+...
     |   :   :   | x
     +...........+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */

    EXPECT_FALSE(gameboardView.nearestUnbuiltWall(dimensions.width * 3 + 1, 0));
}
TEST_F(GameboardViewTest, NewlyCreatedGameboardHasNoRoomsMissingLastWall)
{
    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    EXPECT_TRUE(gameboardView.lastWalls().empty());
}

TEST_F(GameboardViewTest, LastWalls)
{
    // clang-format off
    const GameboardView::Walls walls{
        {{0, 0}, Side::bottom},
        {{2, 2}, Side::left}};
    // clang-format on

    /*
     +---+---+---+
     |   |   :   |
     +...+.......+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    buildWall({0, 0}, Side::right);
    EXPECT_THAT(gameboardView.lastWalls(), ElementsAre(walls[0]));

    /*
     +---+---+---+
     |   |   :   |
     +...+.......+
     |   :   :   |
     +.......+---+
     |   :   :   |
     +---+---+---+
     */
    buildWall({2, 2}, Side::top);
    EXPECT_THAT(gameboardView.lastWalls(), UnorderedElementsAreArray(walls));
}

TEST_F(GameboardViewTest, SafeWalls)
{
    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    EXPECT_EQ(8, gameboardView.safeWalls().size());

    /*
     +---+---+---+
     |   :   :   |
     +...+---+...+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    buildWall({1, 1}, Side::top);
    EXPECT_EQ(6, gameboardView.safeWalls().size());

    /*
     +---+---+---+
     |   :   :   |
     +...+---+...+
     |   |   :   |
     +...+.......+
     |   :   :   |
     +---+---+---+
     */
    buildWall({1, 1}, Side::left);
    EXPECT_TRUE(gameboardView.safeWalls().empty());
}

TEST_F(GameboardViewTest, UnsafeWalls)
{
    // clang-format off
    const GameboardView::Walls walls{
        {{2, 0}, Side::bottom},
        {{2, 2}, Side::left},
        {{1, 1}, Side::top}};
    // clang-format on

    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(8, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(walls[0]));
    EXPECT_THAT(unsafeWalls, Contains(walls[1]));
    EXPECT_THAT(unsafeWalls, Not(Contains(walls[2])));

    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...+---+...+
     |   :   :   |
     +---+---+---+
     */
    buildWall({1, 1}, Side::bottom);
    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(4, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(walls[0]));
    EXPECT_THAT(unsafeWalls, Not(Contains(walls[1])));
    EXPECT_THAT(unsafeWalls, Not(Contains(walls[2])));

    /*
     +---+---+---+
     |   :   :   |
     +.......+...+
     |   :   |   |
     +...+---+...+
     |   :   :   |
     +---+---+---+
     */
    buildWall({1, 1}, Side::right);
    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(4, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Not(Contains(walls[0])));
    EXPECT_THAT(unsafeWalls, Not(Contains(walls[1])));
    EXPECT_THAT(unsafeWalls, Contains(walls[2]));
}

TEST_F(GameboardViewTest, UnsafeWallsWhenAllRoomsHaveAtLeastTwoWalls)
{
    // clang-format off
    const GameboardView::Walls walls{
        {{2, 0}, Side::left},
        {{2, 1}, Side::left},
        {{0, 2}, Side::top},
        {{1, 2}, Side::top}};
    // clang-format on

    /*
     +---+---+---+
     |   :   |   |
     +.......+...+
     |   :   |   |
     +---+---+...+
     |   :   :   |
     +---+---+---+
     */

    std::ranges::for_each(walls, [this](auto l) {
        buildWall(l.position, l.side);
    });

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(8, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{2, 0}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{2, 1}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{0, 2}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{1, 2}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{2, 2}))));
}

TEST_F(GameboardViewTest, HeatDelta)
{
    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    auto heat = gameboardView.heatDelta();
    EXPECT_TRUE(heat.empty()) << "expected empty because nothing has changed";

    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...+---+...+
     |   :   :   |
     +---+---+---+
     */
    buildWall({1, 1}, Side::bottom);
    heat = gameboardView.heatDelta();
    EXPECT_THAT(
        heat,
        UnorderedElementsAre(
            std::make_pair(Point{0, 2}, 3), std::make_pair(Point{1, 2}, 3),
            std::make_pair(Point{2, 2}, 3)));

    /*
     +---+---+---+
     |   :   :   |
     +.......+...+
     |   :   |   |
     +...+---+...+
     |   :   :   |
     +---+---+---+
     */
    buildWall({1, 1}, Side::right);
    heat = gameboardView.heatDelta();
    EXPECT_THAT(
        heat,
        UnorderedElementsAre(
            std::make_pair(Point{2, 0}, 5), std::make_pair(Point{2, 1}, 5),
            std::make_pair(Point{0, 2}, 5), std::make_pair(Point{1, 2}, 5),
            std::make_pair(Point{2, 2}, 5)));
    EXPECT_THAT(heat, Not(Contains(std::make_pair(Point{1, 1}, 1))))
        << "not expected because heat has actually not changed (heat is 1 by default)";
}

// Unit tests to cover issues encountered during heatmap implementation
class GameboardViewHeatmapTest : public Test
{
protected:
    void buildWalls(const GameboardView::Walls& walls)
    {
        std::ranges::for_each(walls, [this](auto wall) {
            buildWall(wall.position, wall.side);
        });
    }

    void buildWall(Point position, Side side)
    {
        gameboard.build({position, side});
    }

    // NOTE: We could just as easily compare two vector-based heatmaps, but converting the
    // heatmap to a string improves readability and allows the test framework to produce a
    // diff that better shows the difference.
    auto toString(const GameboardView::Heatmap& heatmap) const -> std::string
    {
        using namespace std::string_literals;
        auto str = ""s;

        // The gameboard is always square, so we get the width (and height) from the size
        // TODO: With C++23 we could perhaps use std::views::chunk here
        for (int width = std::sqrt(std::size(heatmap)), i = 0; int heat : heatmap)
        {
            if (i++ % width == 0)
                str += "\n";

            str += !heat ? "  -"s : fmt::format("{:>3}", heat);
        }

        return str;
    }

    GameboardView gameboardView;
    Gameboard gameboard{{1, 1, 4}};
};

TEST_F(GameboardViewHeatmapTest, IncludeFirstRoomOnlyOnceWhenNewlyBuiltRoomCreatesLoop)
{
    // In this case, the starting room was included twice in the heat calculation
    // when the newly built wall created a loop of "unsafe" rooms.

    /*
     +---+---+---+---+
     |           |   |
     +   +---+   +   +
     |           |   |
     +---+       +   +
     |   |       |   |
     +   +---+---+   +
     |               |
     +---+---+---+---+
     */
    buildWalls(
        {{{3, 1}, Side::left},
         {{3, 2}, Side::left},
         {{1, 3}, Side::top},
         {{2, 3}, Side::top},
         {{0, 2}, Side::right},
         {{0, 2}, Side::top},
         {{3, 0}, Side::left},
         {{1, 1}, Side::top}});
    EXPECT_EQ(
        "\n  4  4  4  -"
        "\n  4  -  -  8"
        "\n  -  2  2  8"
        "\n  8  8  8  8",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(4, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{2, 2})));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{3, 3}))));

    /*
     +---+---+---+---+
     |           |   |
     +   +---+   +   +
     |       |   |   |
     +---+   +   +   +
     |   |       |   |
     +   +---+---+   +
     |               |
     +---+---+---+---+
     */
    buildWall({2, 1}, Side::left);
    EXPECT_EQ(
        "\n  8  8  8  -"
        "\n  8  8  8  8"
        "\n  -  8  8  8"
        "\n  8  8  8  8",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(28, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{2, 2})));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{3, 3})));
}

TEST_F(GameboardViewHeatmapTest, UpdateHeatWhenWallIsBuiltInUnsafeRoom)
{
    // In this case, the heats were not updated when the rooms on both sides
    // of the newly built wall already had 2 walls.

    /*
     +---+---+---+---+
     |       |       |
     +---+---+       +
     |       |       |
     +---+---+   +---+
     |       |       |
     +   +---+---+   +
     |               |
     +---+---+---+---+
     */
    buildWalls(
        {{{0, 0}, Side::bottom},
         {{1, 1}, Side::top},
         {{2, 0}, Side::left},
         {{3, 2}, Side::top},
         {{2, 3}, Side::top},
         {{1, 1}, Side::right},
         {{2, 2}, Side::left},
         {{1, 2}, Side::top},
         {{0, 1}, Side::bottom},
         {{1, 3}, Side::top}});
    EXPECT_EQ(
        "\n  -  - 12 12"
        "\n  -  -  - 12"
        "\n  8  -  8  8"
        "\n  8  8  8  8",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(14, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{3, 0}))));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 3})));

    /*
     +---+---+---+---+
     |       |       |
     +---+---+       +
     |       |       |
     +---+---+   +---+
     |       |   |   |
     +   +---+---+   +
     |               |
     +---+---+---+---+
     */
    buildWall({3, 2}, Side::left);
    EXPECT_EQ(
        "\n  -  -  5  5"
        "\n  -  -  -  5"
        "\n  7  -  -  -"
        "\n  7  7  7  7",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(6, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{3, 0})));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{0, 3}))));
}

TEST_F(GameboardViewHeatmapTest, DoNotUpdateHeatPastSafeRoomsWhenWallIsBuiltInUnsafeRoom)
{
    // In this case, the list of completable rooms extends past "safe" rooms. However, the
    // heat of the rooms after a "safe" room is not the same as the heat of the rooms before
    // it. The heat of the rooms after the "safe" rooms may also need to be updated, but in
    // this test they should already have the correct heat.

    /*
     +---+---+---+---+
     |       |       |
     +   +   +   +   +
     |   |       |   |
     +   +---+   +   +
     |       |   |   |
     +       +   +   +
     |               |
     +---+---+---+---+
     */
    buildWalls(
        {{{1, 1}, Side::left},
         {{2, 1}, Side::right},
         {{1, 1}, Side::bottom},
         {{1, 2}, Side::right},
         {{2, 0}, Side::left},
         {{2, 2}, Side::right}});
    EXPECT_EQ(
        "\n  4  4  5  5"
        "\n  4  4  -  5"
        "\n  -  1  1  5"
        "\n  1  -  -  5",
        toString(gameboardView.heatmap()));

    /*
     +---+---+---+---+
     |       |       |
     +   +   +   +   +
     |   |       |   |
     +   +---+   +   +
     |       |   |   |
     +       +   +   +
     |       |       |
     +---+---+---+---+
     */
    buildWall({2, 3}, Side::left);
    EXPECT_EQ(
        "\n  4  4 12 12"
        "\n  4  4  - 12"
        "\n  -  8 12 12"
        "\n  8  8 12 12",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(8, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, (Contains(Field(&WallPosition::position, Point{0, 0}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{3, 0}))));

    /*
     +---+---+---+---+
     |       |       |
     +   +   +   +   +
     |   |   |   |   |
     +   +---+   +   +
     |       |   |   |
     +       +   +   +
     |       |       |
     +---+---+---+---+
     */
    buildWall({1, 1}, Side::right);
    EXPECT_EQ(
        "\n  4  4  8  8"
        "\n  4  -  8  8"
        "\n  -  8  8  8"
        "\n  8  8  8  8",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(6, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, (Contains(Field(&WallPosition::position, Point{0, 0}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{3, 0}))));
}

TEST_F(
    GameboardViewHeatmapTest, UpdateHeatBehindNewlyBuiltWallWhenFirstUnbuiltWallIsInUnsafeRoom)
{
    // In this case, the heat was not updated for the rooms behind the newly built wall
    // (upper left corner).

    /*
     +---+---+---+---+
     |       |       |
     +       +   +   +
     |           |   |
     +---+       +   +
     |           |   |
     +   +---+   +   +
     |               |
     +---+---+---+---+
     */
    buildWalls(
        {{{0, 1}, Side::bottom},
         {{1, 0}, Side::right},
         {{1, 3}, Side::top},
         {{3, 1}, Side::left},
         {{3, 2}, Side::left}});
    EXPECT_EQ(
        "\n  3  3  5  5"
        "\n  3  -  -  5"
        "\n  3  -  -  5"
        "\n  3  3  -  5",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(12, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, (Contains(Field(&WallPosition::position, Point{0, 3}))));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 0})));

    /*
     +---+---+---+---+
     |       |       |
     +       +   +   +
     |       |   |   |
     +---+   +   +   +
     |           |   |
     +   +---+   +   +
     |               |
     +---+---+---+---+
     */
    buildWall({2, 1}, Side::left);
    EXPECT_EQ(
        "\n  4  4  6  6"
        "\n  4  -  6  6"
        "\n  3  -  -  6"
        "\n  3  3  -  6",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(6, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, (Contains(Field(&WallPosition::position, Point{0, 3}))));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{0, 0}))));
}

TEST_F(
    GameboardViewHeatmapTest,
    UpdateHeatBehindNewlyBuiltWallWhenFirstUnbuiltWallIsNotInUnsafeRoom)
{
    // In this case, the heat was not updated to the rooms behind the newly built wall
    // (bottom left corner), even after fixing the "mirror" test above. This was because
    // the first unbuilt wall (top wall) was in a "safe" room (unlike in the above test).
    // (The algorithm returns the walls in the following order when the first unbuilt wall
    // is requested: top -> left -> bottom -> right).

    /*
     +---+---+---+---+
     |               |
     +   +---+   +   +
     |           |   |
     +---+       +   +
     |           |   |
     +       +   +   +
     |       |       |
     +---+---+---+---+
     */
    buildWalls(
        {{{0, 1}, Side::bottom},
         {{1, 0}, Side::bottom},
         {{3, 1}, Side::left},
         {{2, 2}, Side::right},
         {{1, 3}, Side::right}});
    EXPECT_EQ(
        "\n  3  3  -  5"
        "\n  3  -  -  5"
        "\n  3  -  -  5"
        "\n  3  3  5  5",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(12, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 0})));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 3})));

    /*
     +---+---+---+---+
     |               |
     +   +---+   +   +
     |           |   |
     +---+   +   +   +
     |       |   |   |
     +       +   +   +
     |       |       |
     +---+---+---+---+
     */
    buildWall({2, 2}, Side::left);
    EXPECT_EQ(
        "\n  3  3  -  6"
        "\n  3  -  -  6"
        "\n  4  -  6  6"
        "\n  4  4  6  6",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(6, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 0})));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{0, 3}))));
}

TEST_F(
    GameboardViewHeatmapTest, ReverseUpdateHeatBehindSafeRoomWhenWhenWallIsBuiltInUnsafeRoom)
{
    // In this case, the heat in the upper left corner was not updated when
    // a new wall was built in the lower left corner.

    /*
     +---+---+---+---+
     |       |       |
     +       +   +   +
     |       |   |   |
     +   +---+   +   +
     |       |   |   |
     +       +   +   +
     |       |       |
     +---+---+---+---+
     */
    buildWalls(
        {{{3, 1}, Side::left},
         {{1, 1}, Side::bottom},
         {{2, 1}, Side::left},
         {{1, 2}, Side::right},
         {{1, 0}, Side::right},
         {{2, 2}, Side::right},
         {{2, 3}, Side::left}});
    EXPECT_EQ(
        "\n  4  4  8  8"
        "\n  -  4  8  8"
        "\n  -  4  8  8"
        "\n  4  4  8  8",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(12, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 0})));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 3})));

    /*
     +---+---+---+---+
     |       |       |
     +       +   +   +
     |       |   |   |
     +   +---+   +   +
     |   |   |   |   |
     +   +   +   +   +
     |       |       |
     +---+---+---+---+
     */
    buildWall({1, 2}, Side::left);
    EXPECT_EQ(
        "\n  8  8  8  8"
        "\n  -  8  8  8"
        "\n  4  -  8  8"
        "\n  4  4  8  8",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(6, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{0, 0}))));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 3})));
}

TEST_F(GameboardViewHeatmapTest, UpdateHeatOfAllNeighboringRoomsWhenWallIsBuiltInSafeRoom)
{
    // In this case, the room where the new wall was built remains "safe",
    // but all rooms behind the unbuilt walls are "unsafe" and would require
    // a heat update. However, only the heat of the first "unsafe" room was updated
    // (which didn't even need an update).

    /*
     +---+---+---+---+
     |               |
     +   +---+---+   +
     |       |       |
     +---+   +---+---+
     |               |
     +       +       +
     |       |       |
     +---+---+---+---+
     */
    buildWalls(
        {{{1, 1}, Side::top},
         {{2, 1}, Side::top},
         {{2, 1}, Side::left},
         {{1, 3}, Side::right},
         {{2, 1}, Side::bottom},
         {{0, 2}, Side::top},
         {{3, 1}, Side::bottom}});
    EXPECT_EQ(
        "\n  8  8  8  8"
        "\n  8  8  -  8"
        "\n  3  -  -  4"
        "\n  3  3  4  4",
        toString(gameboardView.heatmap()));

    auto unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(6, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{0, 3})));
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{3, 3}))));

    /*
     +---+---+---+---+
     |               |
     +   +---+---+   +
     |       |       |
     +---+   +---+---+
     |       |       |
     +       +       +
     |       |       |
     +---+---+---+---+
     */
    buildWall({1, 2}, Side::right);
    EXPECT_EQ(
        "\n  8  8  8  8"
        "\n  8  8  -  8"
        "\n 12  -  4  4"
        "\n 12 12  4  4",
        toString(gameboardView.heatmap()));

    unsafeWalls = gameboardView.unsafeWalls();
    EXPECT_EQ(8, unsafeWalls.size());
    EXPECT_THAT(unsafeWalls, Not(Contains(Field(&WallPosition::position, Point{0, 3}))));
    EXPECT_THAT(unsafeWalls, Contains(Field(&WallPosition::position, Point{3, 3})));
}

TEST_F(
    GameboardViewHeatmapTest, MarkUnsafeRoomAsSafeWhenBothNeighboringRoomsAreMissingLastWall)
{
    // An unsafe room with only one wall missing from both neighboring rooms is essentially a
    // "safe" room, meaning building a wall in this room will only complete one of the other
    // rooms, allowing the current player to continue playing.

    /*
     +---+---+---+---+
     |       |       |
     +   +---+       +
     |   |           |
     +---+           +
     |               |
     +               +
     |               |
     +---+---+---+---+
     */
    buildWalls(
        {{{1, 0}, Side::right},
         {{0, 1}, Side::bottom},
         {{1, 1}, Side::top},
         {{1, 1}, Side::left}});
    EXPECT_EQ(
        "\n  1  -  2  2"
        "\n  -  1  -  -"
        "\n  2  -  -  -"
        "\n  2  -  -  1",
        toString(gameboardView.heatmap()));
}

TEST_F(GameboardViewHeatmapTest, UpdateHeatmap)
{
    // In this case, there was an error in the location comparison function,
    // which resulted in some rooms being counted twice in the heat calculation.

    gameboardView = GameboardView{};
    gameboard = Gameboard{{1, 1, 7}};

    /*
     +---+---+---+---+---+---+---+
     |           |               |
     +       +   +   +---+---+   +
     |       |                   |
     +   +---+---+---+---+---+---+
     |   |       |               |
     +   +   +   +   +---+---+   +
     |   |   |   |           |   |
     +   +   +   +   +---+   +   +
     |   |       |           |   |
     +   +---+---+---+---+---+   +
     |   |       |               |
     +   +       +   +---+---+   +
     |           |               |
     +---+---+---+---+---+---+---+
     */
    buildWalls({{{4, 2}, Side::top},    {{5, 5}, Side::top},    {{4, 5}, Side::bottom},
                {{2, 2}, Side::top},    {{0, 2}, Side::right},  {{5, 6}, Side::top},
                {{2, 0}, Side::right},  {{2, 6}, Side::right},  {{3, 5}, Side::left},
                {{3, 4}, Side::bottom}, {{4, 3}, Side::top},    {{3, 2}, Side::left},
                {{2, 3}, Side::left},   {{4, 3}, Side::bottom}, {{1, 1}, Side::right},
                {{4, 4}, Side::bottom}, {{6, 1}, Side::bottom}, {{1, 4}, Side::left},
                {{2, 3}, Side::right},  {{1, 4}, Side::bottom}, {{5, 4}, Side::right},
                {{3, 4}, Side::left},   {{5, 1}, Side::top},    {{0, 5}, Side::right},
                {{5, 1}, Side::bottom}, {{0, 3}, Side::right},  {{4, 0}, Side::bottom},
                {{1, 2}, Side::top},    {{3, 1}, Side::bottom}, {{5, 3}, Side::top},
                {{5, 3}, Side::right},  {{2, 4}, Side::bottom}});
    EXPECT_EQ(
        "\n  1  -  2 10 10 10 10"
        "\n  -  1  2  - 10 10 10"
        "\n  5  6  6  6  6  6  6"
        "\n  5  6  6  - 12 12  6"
        "\n  5  6  6 12 12 12  6"
        "\n  5  9  9 14 14 14  -"
        "\n  5  -  9 14 14 14 14",
        toString(gameboardView.heatmap()));

    /*
     +---+---+---+---+---+---+---+
     |           |               |
     +   +---+   +   +---+---+   +
     |       |                   |
     +   +---+---+---+---+---+---+
     |   |       |               |
     +   +   +   +   +---+---+   +
     |   |   |   |           |   |
     +   +   +   +   +---+   +   +
     |   |       |           |   |
     +   +---+---+---+---+---+   +
     |   |       |               |
     +   +       +   +---+---+   +
     |           |               |
     +---+---+---+---+---+---+---+
     */
    buildWall({1, 1}, Side::top);
    EXPECT_EQ(
        "\n  4  4  4 12 12 12 12"
        "\n  -  -  4  - 12 12 12"
        "\n  5  6  6  6  6  6  6"
        "\n  5  6  6  - 12 12  6"
        "\n  5  6  6 12 12 12  6"
        "\n  5  9  9 14 14 14  -"
        "\n  5  -  9 14 14 14 14",
        toString(gameboardView.heatmap()));
}
} // namespace
} // namespace rooms::gameboard
