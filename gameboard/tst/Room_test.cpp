// Copyright (c) 2020 Antero Nousiainen

#include "Room.hpp"
#include <signals/ScopedConnection.hpp>
#include <gmock/gmock.h>

namespace rooms::gameboard
{
namespace
{
using namespace testing;

class RoomTest : public Test
{
public:
    MOCK_METHOD(void, onWallBuilt, (Point, Side), (const));

protected:
    static auto built(const Room& room) -> decltype(room.asTile().built)
    {
        return room.asTile().built;
    }

    static auto layout(const Room& room) -> decltype(room.asTile().layout)
    {
        return room.asTile().layout;
    }

    static auto position(const Room& room) -> decltype(room.asTile().position)
    {
        return room.asTile().position;
    }

    const Rect middle{5, 5, 5, 5};
    Room room{middle};

    Room onTop{middle.translated(0, -middle.height())};
    Room onLeft{middle.translated(-middle.width(), 0)};
    Room onBottom{middle.translated(0, middle.height())};
    Room onRight{middle.translated(middle.width(), 0)};
    Room onFarRight{middle.translated(middle.width() * 2, 0)};
};

TEST_F(RoomTest, IsCopyableAndMovable)
{
    // There is no need to copy and/or move rooms (except in unit tests),
    // but on the other hand, there is no reason to prevent these operations either.
    EXPECT_TRUE(std::is_copy_constructible_v<Room>);
    EXPECT_TRUE(std::is_copy_assignable_v<Room>);
    EXPECT_TRUE(std::is_move_constructible_v<Room>);
    EXPECT_TRUE(std::is_move_assignable_v<Room>);
}

TEST_F(RoomTest, Layout)
{
    EXPECT_EQ(middle, layout(room));
    EXPECT_EQ((Rect{2, 3, 5, 7}), layout(Room{Rect{2, 3, 5, 7}}));
}

TEST_F(RoomTest, Position)
{
    EXPECT_EQ((Point{1, 1}), position(room));
    EXPECT_EQ((Point{1, 0}), position(onTop));
    EXPECT_EQ((Point{0, 1}), position(onLeft));
}

TEST_F(RoomTest, AllWallsAreBuiltByDefault)
{
    using enum Side;
    EXPECT_THAT(built(room), ElementsAre(top, left, bottom, right));
}

TEST_F(RoomTest, IsCompletedByDefault)
{
    // The room is completed when all its walls are built,
    // and since all the walls are built by default,
    // the room is completed by default.
    EXPECT_TRUE(room.completed());
}

TEST_F(RoomTest, ConnectingWallIsUnbuiltWhenRoomsAreLinked)
{
    /*
     +---+---+
     |   :   |
     +---+---+
     */
    using enum Side;
    room.link(onLeft, left);

    EXPECT_THAT(built(room), Not(Contains(left)));
    EXPECT_THAT(built(onLeft), Not(Contains(right)));

    // To ensure that the other walls are still built
    EXPECT_THAT(built(room), ElementsAre(top, bottom, right));
    EXPECT_THAT(built(onLeft), ElementsAre(top, left, bottom));
}

TEST_F(RoomTest, IsNotCompletedWhenLinked)
{
    room.link(onLeft, Side::left);

    EXPECT_FALSE(room.completed());
    EXPECT_FALSE(onLeft.completed());
}

TEST_F(RoomTest, IsCompletedWhenConnectingWallsAreBuilt)
{
    /*
     +---+---+---+
     |   :   :   |
     +---+---+---+
     */
    room.link(onLeft, Side::left);
    room.link(onRight, Side::right);

    EXPECT_FALSE(onLeft.completed());
    EXPECT_FALSE(room.completed());
    EXPECT_FALSE(onRight.completed());

    /*
     +---+---+---+
     | x |   :   |
     +---+---+---+
     */
    onLeft.build(Side::right);

    EXPECT_TRUE(onLeft.completed());
    EXPECT_FALSE(room.completed());
    EXPECT_FALSE(onRight.completed());

    /*
     +---+---+---+
     |   |   | x |
     +---+---+---+
     */
    onRight.build(Side::left);

    EXPECT_TRUE(onLeft.completed());
    EXPECT_TRUE(room.completed());
    EXPECT_TRUE(onRight.completed());
}

TEST_F(RoomTest, NotifySubscribersWhenWallIsBuilt)
{
    /*
     +---+---+---+---+
     |   :   :   :   |
     +---+---+---+---+
     */
    room.link(onLeft, Side::left);
    room.link(onRight, Side::right);
    onRight.link(onFarRight, Side::right);

    signals::ScopedConnection scopedSubscription =
        WallBuiltEvent::subscribe([this](const WallPosition& wall) {
            onWallBuilt(wall.position, wall.side);
        });

    /*
     +---+---+---+---+
     |   : x |   :   |
     +---+---+---+---+
     */
    EXPECT_CALL(*this, onWallBuilt(position(room), Side::right));
    room.build(Side::right);

    /*
     +---+---+---+---+
     |   | x |   :   |
     +---+---+---+---+
     */
    EXPECT_CALL(*this, onWallBuilt(position(room), Side::left));
    room.build(Side::left);
}

TEST_F(RoomTest, CanThrowAndCatchCompletedByNonPlayer)
{
    const auto throwCompletedByNonPlayer = [] {
        throw Room::CompletedByNonPlayer{};
    };
    EXPECT_THAT(
        throwCompletedByNonPlayer,
        ThrowsMessage<GameboardError>(StrEq("room completed by a non-player")));
}
} // namespace
} // namespace rooms::gameboard
