// Copyright (c) 2022 Antero Nousiainen

#include <gameboard/GameboardError.hpp>
#include <gmock/gmock.h>

namespace rooms::gameboard
{
namespace
{
using namespace testing;

TEST(GameboardErrorTest, CanThrowAndCatchGameboardError)
{
    const auto throwGameboardError = [] {
        throw GameboardError{"what"};
    };
    EXPECT_THAT(throwGameboardError, ThrowsMessage<std::runtime_error>(StrEq("what")));
}
} // namespace
} // namespace rooms::gameboard
