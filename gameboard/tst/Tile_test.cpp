// Copyright (c) 2022 Antero Nousiainen

#include <gameboard/Tile.hpp>
#include <gtest/gtest.h>

namespace rooms::gameboard
{
namespace
{
TEST(TileTest, IsNotSmallEnoughToPassByValue)
{
    // "I often hear the question in my seminars: What is cheaply copyable?
    //  The guidelines are pretty concrete.
    //
    //  * You should not copy the parameter p if sizeof(p) > 4 * sizeof(int)
    //  * You should not use a const reference to p if sizeof(p) < 3 * sizeof(int)
    //
    //  I assume these numbers are based on experience."
    // https://www.modernescpp.com/index.php/c-core-guidelines-how-to-pass-function-parameters/
    EXPECT_GT(sizeof(Tile), 4 * sizeof(int));
}
} // namespace
} // namespace rooms::gameboard
