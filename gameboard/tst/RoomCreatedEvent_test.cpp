// Copyright (c) 2021 Antero Nousiainen

#include <gameboard/RoomCreatedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::gameboard
{
namespace
{
TEST(RoomCreatedEventTest, SubscribeEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        RoomCreatedEvent::subscribe([&subscriberInvoked](const Tile&) {
            subscriberInvoked = true;
        });

    std::invoke(RoomCreatedEvent{}, Tile{});
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::gameboard
