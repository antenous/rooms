// Copyright (c) 2021 Antero Nousiainen

#include <gameboard/Gameboard.hpp>
#include <gameboard/WallBuiltEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gmock/gmock.h>
#include "Room.hpp"
#include "Wall.hpp"

namespace rooms::gameboard
{
namespace
{
using namespace testing;

class GameboardTest : public Test
{
protected:
    void buildWall(Point position, Side side)
    {
        gameboard.build({position, side});
    }

    const int width = 7;
    const int height = 11;
    Gameboard gameboard{{width, height, 2}};
};

TEST_F(GameboardTest, IsNonCopyable)
{
    // Gameboard is non-copyable because of the pimpl idiom
    EXPECT_FALSE(std::is_copy_constructible_v<Gameboard>);
    EXPECT_FALSE(std::is_copy_assignable_v<Gameboard>);
}

TEST_F(GameboardTest, IsMovable)
{
    EXPECT_TRUE(std::is_move_constructible_v<Gameboard>);
    EXPECT_TRUE(std::is_move_assignable_v<Gameboard>);
}

TEST_F(GameboardTest, Move)
{
    const auto other = std::move(gameboard);
    EXPECT_FALSE(other.gameOver());

    gameboard = Gameboard{{1, 1, 1}};
    EXPECT_TRUE(gameboard.gameOver());
}

TEST_F(GameboardTest, NotifyListenersOfNewRoomsWhenGameboardIsCreated)
{
    auto roomsCreated = 0;

    signals::ScopedConnection scopedSubscription =
        RoomCreatedEvent::subscribe([&roomsCreated](const Tile&) {
            ++roomsCreated;
        });

    const auto size = 42;
    Gameboard{{width, height, size}};
    EXPECT_EQ(size * size, roomsCreated);
}

TEST_F(GameboardTest, CreateRoomLayoutsUsingWidthAndHeight)
{
    std::vector<Rect> layouts;

    signals::ScopedConnection subscription =
        RoomCreatedEvent::subscribe([&layouts](const Tile& tile) {
            layouts.emplace_back(tile.layout);
        });

    Gameboard{{width, height, 3}};

    EXPECT_EQ((Rect{0, 0, width, height}), layouts[0]);
    EXPECT_EQ((Rect{width, height, width, height}), layouts[4]);
}

TEST_F(GameboardTest, TileInEventsContainListOfBuiltWalls)
{
    auto builtWalls = std::vector<Tile::Sides>{};

    signals::ScopedConnection subscription =
        RoomCreatedEvent::subscribe([&builtWalls](const Tile& tile) {
            builtWalls.emplace_back(tile.built);
        });

    Gameboard{{width, height, 3}};

    EXPECT_THAT(builtWalls[0], ElementsAre(Side::top, Side::left));
    EXPECT_THAT(builtWalls[1], ElementsAre(Side::top));
    EXPECT_THAT(builtWalls[4], IsEmpty());
}

TEST_F(GameboardTest, AdjoiningRoomsShareWalls)
{
    buildWall({0, 0}, Side::right);
    EXPECT_THROW(buildWall({1, 0}, Side::left), Wall::AlreadyBuilt);

    buildWall({0, 1}, Side::top);
    EXPECT_THROW(buildWall({0, 0}, Side::bottom), Wall::AlreadyBuilt);
}

TEST_F(GameboardTest, SharedWallIsUnbuilt)
{
    auto wallsBuilt = 0;
    signals::ScopedConnection subscription = WallBuiltEvent::subscribe([&wallsBuilt](auto) {
        ++wallsBuilt;
    });

    buildWall({0, 0}, Side::right);
    buildWall({0, 0}, Side::bottom);

    EXPECT_EQ(2, wallsBuilt);
}

TEST_F(GameboardTest, GameIsNotOverByDefault)
{
    EXPECT_FALSE(gameboard.gameOver());
}

TEST_F(GameboardTest, GameIsOverWhenRoomsAreComplete)
{
    const auto topLeft = Point{0, 0};
    const auto bottomRight = Point{1, 1};

    buildWall(topLeft, Side::right);
    buildWall(topLeft, Side::bottom);

    buildWall(bottomRight, Side::left);
    buildWall(bottomRight, Side::top);

    EXPECT_TRUE(gameboard.gameOver());
}

TEST_F(GameboardTest, CanThrowAndCatchPositionOffGameboard)
{
    const auto throwPositionOffGameboard = [] {
        throw Gameboard::PositionOffGameboard{};
    };
    EXPECT_THAT(
        throwPositionOffGameboard,
        ThrowsMessage<GameboardError>(StrEq("position off gameboard")));
}

TEST_F(GameboardTest, ThrowWhenPositionIsOffGameboard)
{
    EXPECT_THROW(buildWall({0, 2}, Side::left), Gameboard::PositionOffGameboard);
    EXPECT_THROW(buildWall({2, 0}, Side::top), Gameboard::PositionOffGameboard);
}

} // namespace
} // namespace rooms::gameboard
