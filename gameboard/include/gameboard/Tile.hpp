// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_TILE_HPP_
#define ROOMS_GAMEBOARD_TILE_HPP_

#include <common/Point.hpp>
#include <common/Rect.hpp>
#include <common/Side.hpp>
#include <vector>

namespace rooms::gameboard
{

struct Tile
{
    using Sides = std::vector<Side>;

    Point position;

    Rect layout;

    Sides built;
};

} // namespace rooms::gameboard

#endif
