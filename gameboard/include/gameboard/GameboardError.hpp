// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_GAMEBOARDERROR_HPP_
#define ROOMS_GAMEBOARD_GAMEBOARDERROR_HPP_

#include <stdexcept>

namespace rooms::gameboard
{

class GameboardError : public std::runtime_error
{
public:
    using runtime_error::runtime_error;
};

} // namespace rooms::gameboard

#endif
