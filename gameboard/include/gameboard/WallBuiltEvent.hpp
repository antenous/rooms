// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_WALLBUILTEVENT_HPP_
#define ROOMS_GAMEBOARD_WALLBUILTEVENT_HPP_

#include <common/WallPosition.hpp>
#include <signals/Event.hpp>

namespace rooms::gameboard
{

struct WallBuiltEvent : signals::Event<WallBuiltEvent, void(const WallPosition&)>
{
};

} // namespace rooms::gameboard

#endif
