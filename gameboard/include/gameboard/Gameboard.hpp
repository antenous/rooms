// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_GAMEBOARD_HPP_
#define ROOMS_GAMEBOARD_GAMEBOARD_HPP_

#include "GameboardError.hpp"
#include "RoomCreatedEvent.hpp"
#include <common/Dimensions.hpp>
#include <common/WallPosition.hpp>
#include <memory>

namespace rooms::gameboard
{

class Gameboard
{
public:
    class PositionOffGameboard;

    explicit Gameboard(const Dimensions& dimensions);

    Gameboard(const Gameboard&) = delete;

    Gameboard(Gameboard&& other);

    ~Gameboard();

    Gameboard& operator=(const Gameboard&) = delete;

    Gameboard& operator=(Gameboard&& other);

    auto build(const WallPosition& wall) -> std::vector<Point>;

    bool gameOver() const;

private:
    class GameboardImpl;

    std::unique_ptr<GameboardImpl> pimpl;
};

class Gameboard::PositionOffGameboard : public GameboardError
{
public:
    PositionOffGameboard();
};

} // namespace rooms::gameboard

#endif
