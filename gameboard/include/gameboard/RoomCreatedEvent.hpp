// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_ROOMCREATEDEVENT_HPP_
#define ROOMS_GAMEBOARD_ROOMCREATEDEVENT_HPP_

#include "Tile.hpp"
#include <signals/Event.hpp>

namespace rooms::gameboard
{

struct RoomCreatedEvent : signals::Event<RoomCreatedEvent, void(const Tile&)>
{
};

} // namespace rooms::gameboard

#endif
