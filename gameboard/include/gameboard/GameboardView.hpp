// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_GAMEBOARDVIEW_HPP_
#define ROOMS_GAMEBOARD_GAMEBOARDVIEW_HPP_

#include <common/Rect.hpp>
#include <common/WallPosition.hpp>
#include <memory>
#include <optional>
#include <vector>

namespace rooms::gameboard
{

/**
 Gameboard view is a read-only view of the gameboard that is
 specifically designed to meet the needs of the computer player.

 @note
 Because the gameboard view is based on room and wall events, it must
 be instantiated before the gameboard for it to function properly.
 */
class GameboardView
{
public:
    using Heatmap = std::vector<int>;

    using HeatDelta = std::vector<std::pair<Point, int>>;

    using Walls = std::vector<WallPosition>;

    GameboardView();

    GameboardView(const GameboardView&) = delete;

    GameboardView(GameboardView&& other);

    ~GameboardView();

    GameboardView& operator=(const GameboardView&) = delete;

    GameboardView& operator=(GameboardView&& oher);

    /**
     Returns the nearest unbuilt wall, if any.
     */
    auto nearestUnbuiltWall(int x, int y) const -> std::optional<WallPosition>;

    /**
     Returns the layout of a room based on its position.
     */
    Rect layout(Point position) const;

    /**
     Returns a list of walls needed to complete rooms.
     */
    Walls lastWalls() const;

    /**
     Returns a list of walls that can be built without
     the rooms being completed the next turn.
     */
    Walls safeWalls() const;

    /**
     Returns a list of the walls that will result in the
     least number of rooms being completed in the next turn.
     */
    Walls unsafeWalls() const;

    /**
     Returns a heatmap showing how many rooms the opponent
     could complete if a wall was built in an unsafe room.
     */
    auto heatmap() const -> Heatmap;

    /**
     Returns the change in heatmap since the last wall was built.
     */
    auto heatDelta() const -> HeatDelta;

private:
    class GameboardViewImpl;

    std::unique_ptr<GameboardViewImpl> pimpl;
};

} // namespace rooms::gameboard

#endif
