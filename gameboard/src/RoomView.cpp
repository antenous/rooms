// Copyright (c) 2023 Antero Nousiainen

#include "RoomView.hpp"
#include <common/Utility.hpp>
#include <ranges>

namespace rooms::gameboard
{

RoomView::RoomView(const Tile& tile) :
    tile(tile)
{
    std::ranges::for_each(tile.built, [this](Side side) {
        build(side);
    });
}

void RoomView::build(Side side)
{
    walls[to_underlying(side)] = true;
}

bool RoomView::built(Side side) const
{
    return walls[to_underlying(side)];
}

bool RoomView::contains(int x, int y) const
{
    return tile.layout.contains(x, y);
}

auto RoomView::nearestUnbuiltWall(int x, int y) const -> std::optional<WallPosition>
{
    if (auto unbuilt = unbuiltWalls(); !unbuilt.empty())
        return *std::ranges::min_element(unbuilt, [this, x, y](auto& lhs, auto& rhs) {
            return distance(lhs.side, x, y) < distance(rhs.side, x, y);
        });

    return std::nullopt;
}

int RoomView::distance(Side side, int x, int y) const
{
    // https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
    const auto line = tile.layout | extract(side);
    return std::abs(line.width() * (line.top() - y) - line.height() * (line.left() - x));
}

bool RoomView::isMissingLastWall() const
{
    return wallsBuilt() == 3;
}

bool RoomView::isUnsafeToBuildWallIn() const
{
    return wallsBuilt() == 2;
}

bool RoomView::isSafeToBuildWallIn() const
{
    return wallsBuilt() <= 1;
}

auto RoomView::layout() const -> Rect
{
    return tile.layout;
}

int RoomView::wallsBuilt() const
{
    return std::ranges::count(walls, true);
}

auto RoomView::firstUnbuilt() const -> Walls::value_type
{
    return {
        tile.position,
        static_cast<Side>(std::distance(std::begin(walls), std::ranges::find(walls, false)))};
}

auto RoomView::unbuiltWalls() const -> Walls
{
    using enum Side;
    Walls unbuilt;

    for (auto side : {top, left, bottom, right})
        if (!built(side))
            unbuilt.emplace_back(tile.position, side);

    return unbuilt;
}

int RoomView::heat() const
{
    // NOTE: Rooms with only one wall missing may not have "heat" up to date
    // (and the value is for "unsafe" rooms anyway), hence the check below.
    return isUnsafeToBuildWallIn() ? roomsCompletableIfThirdWallIsBuilt : 0;
}

void RoomView::set(int heat)
{
    roomsCompletableIfThirdWallIsBuilt = heat;
}

} // namespace rooms::gameboard
