// Copyright (c) 2021 Antero Nousiainen

#include "gameboard/Gameboard.hpp"
#include "Room.hpp"
#include <ranges>
#include <vector>

namespace rooms::gameboard
{
namespace
{
bool isLeftmost(Point position)
{
    return position.x() == 0;
}

int isTopmost(Point position)
{
    return position.y() == 0;
}
} // namespace

Gameboard::PositionOffGameboard::PositionOffGameboard() :
    GameboardError("position off gameboard")
{
}

class Gameboard::GameboardImpl
{
public:
    explicit GameboardImpl(const Dimensions& dimensions);

    auto build(const WallPosition& wall) -> std::vector<Point>;

    bool gameOver() const;

private:
    using Rooms = std::vector<std::shared_ptr<Room>>;

    static auto createRooms(const Dimensions& dimensions) -> Rooms;

    void linkRooms() const;

    void linkRooms(Point position) const;

    void connectToRoomOnLeft(Point position) const;

    void connectToRoomOnTop(Point position) const;

    void notifyListeners() const;

    Room& room(Point pos) const;

    Room& room(int x, int y) const;

    Rect grid;
    Rooms rooms;
    RoomCreatedEvent roomCreated{};
};

Gameboard::Gameboard(const Dimensions& dimensions) :
    pimpl(std::make_unique<GameboardImpl>(dimensions))
{
}

Gameboard::GameboardImpl::GameboardImpl(const Dimensions& dimensions) :
    grid(0, 0, dimensions.size, dimensions.size),
    rooms(createRooms(dimensions))
{
    linkRooms();
    notifyListeners();
}

auto Gameboard::GameboardImpl::createRooms(const Dimensions& dimensions) -> Rooms
{
    const auto [width, height, size] = dimensions;
    auto rooms = Rooms{};
    rooms.reserve(size * size);

    for (int y = 0; y < size; ++y)
        for (int x = 0; x < size; ++x)
            rooms.emplace_back(
                std::make_shared<Room>(Rect{x * width, y * height, width, height}));

    return rooms;
}

void Gameboard::GameboardImpl::linkRooms() const
{
    for (auto pos = grid.topLeft(); pos.y() < grid.height(); ++pos.ry())
        for (pos.rx() = grid.x(); pos.x() < grid.width(); ++pos.rx())
            linkRooms(pos);
}

void Gameboard::GameboardImpl::linkRooms(Point position) const
{
    connectToRoomOnLeft(position);
    connectToRoomOnTop(position);
}

void Gameboard::GameboardImpl::connectToRoomOnLeft(Point position) const
{
    if (isLeftmost(position))
        return;

    room(position).link(room(position - Point{1, 0}), Side::left);
}

void Gameboard::GameboardImpl::connectToRoomOnTop(Point position) const
{
    if (isTopmost(position))
        return;

    room(position).link(room(position - Point{0, 1}), Side::top);
}

void Gameboard::GameboardImpl::notifyListeners() const
{
    std::ranges::for_each(
        rooms | std::views::transform(std::mem_fn(&Room::asTile)), [this](const Tile& tile) {
            roomCreated(tile);
        });
}

Gameboard::Gameboard(Gameboard&&) = default;

Gameboard::~Gameboard() = default;

Gameboard& Gameboard::operator=(Gameboard&&) = default;

auto Gameboard::build(const WallPosition& wall) -> std::vector<Point>
{
    return pimpl->build(wall);
}

auto Gameboard::GameboardImpl::build(const WallPosition& wall) -> std::vector<Point>
{
    const auto [position, side] = wall;
    return room(position).build(side);
}

Room& Gameboard::GameboardImpl::room(Point position) const
{
    if (!grid.contains(position))
        throw PositionOffGameboard{};

    return *rooms[position.y() * grid.width() + position.x()];
}

bool Gameboard::gameOver() const
{
    return pimpl->gameOver();
}

bool Gameboard::GameboardImpl::gameOver() const
{
    return std::ranges::all_of(rooms, std::mem_fn(&Room::completed));
}
} // namespace rooms::gameboard
