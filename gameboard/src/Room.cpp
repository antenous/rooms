// Copyright (c) 2005 Antero Nousiainen

#include "Room.hpp"
#include "Wall.hpp"
#include <common/Utility.hpp>
#include <ranges>

namespace rooms::gameboard
{
namespace
{
auto createWalls(const Room& owner)
{
    auto walls = Room::Walls{};
    std::ranges::generate(walls, [&owner] {
        return std::make_shared<Wall>(owner);
    });
    return walls;
}
} // namespace

Room::CompletedByNonPlayer::CompletedByNonPlayer() :
    GameboardError("room completed by a non-player")
{
}

Room::Room(const Rect& layout) :
    layout(layout),
    walls(createWalls(*this))
{
}

void Room::link(const Room& linked, Side side)
{
    const auto i = rooms::to_underlying(side);
    walls[i] = linked.walls[(i + 2) % 4];
    walls[i]->link(*this);
}

auto Room::asTile() const -> Tile
{
    return {position(), layout, built()};
}

auto Room::position() const -> Point
{
    return {layout.x() / layout.width(), layout.y() / layout.height()};
}

auto Room::built() const -> Sides
{
    using enum Side;

    auto builtWalls = Sides{};
    builtWalls.reserve(walls.size());

    std::ranges::copy_if(
        std::array{top, left, bottom, right}, std::back_inserter(builtWalls),
        [this](Side side) {
            return built(side);
        });

    return builtWalls;
}

bool Room::built(Side side) const
{
    return wall(side)->built();
}

auto Room::build(Side side) -> std::vector<Point>
{
    wall(side)->build();
    wallBuilt({position(), side});

    auto completed = std::vector<Point>{};
    std::ranges::transform(
        wall(side)->linked() | std::views::filter(std::mem_fn(&Room::completed)),
        std::back_inserter(completed), std::mem_fn(&Room::position));

    return completed;
}

auto Room::wall(Side side) const -> Walls::value_type
{
    return walls[rooms::to_underlying(side)];
}

bool Room::completed() const
{
    return std::ranges::all_of(walls, std::mem_fn(&Wall::built));
}
} // namespace rooms::gameboard
