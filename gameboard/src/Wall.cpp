// Copyright (c) 2005 Antero Nousiainen

#include "Wall.hpp"

namespace rooms::gameboard
{
Wall::AlreadyBuilt::AlreadyBuilt() :
    GameboardError("wall already built")
{
}

Wall::Wall(const Room& owner)
{
    rooms.front() = &owner;
}

void Wall::link(const Room& other)
{
    rooms.back() = &other;
    isBuilt = false;
}

auto Wall::linked() const -> const Rooms&
{
    return rooms;
}

void Wall::build()
{
    if (isBuilt)
        throw AlreadyBuilt{};

    isBuilt = true;
}

bool Wall::built() const
{
    return isBuilt;
}
} // namespace rooms::gameboard
