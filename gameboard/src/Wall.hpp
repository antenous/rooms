// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_WALL_HPP_
#define ROOMS_GAMEBOARD_WALL_HPP_

#include "gameboard/GameboardError.hpp"
#include <array>

namespace rooms::gameboard
{

class Room;

class Wall
{
public:
    class AlreadyBuilt;

    using Rooms = std::array<const Room*, 2>;

    explicit Wall(const Room& owner);

    void build();

    bool built() const;

    void link(const Room& other);

    auto linked() const -> const Rooms&;

private:
    bool isBuilt = true;
    Rooms rooms{};
};

class Wall::AlreadyBuilt : public GameboardError
{
public:
    AlreadyBuilt();
};
} // namespace rooms::gameboard

#endif
