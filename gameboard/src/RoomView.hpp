// Copyright (c) 2023 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_ROOMVIEW_HPP_
#define ROOMS_GAMEBOARD_ROOMVIEW_HPP_

#include "gameboard/GameboardView.hpp"
#include "gameboard/Tile.hpp"
#include <common/Point.hpp>
#include <common/Side.hpp>
#include <array>
#include <optional>

namespace rooms::gameboard
{

class RoomView
{
public:
    using Walls = GameboardView::Walls;

    RoomView(const Tile& tile);

    void build(Side side);

    bool contains(int x, int y) const;

    auto nearestUnbuiltWall(int x, int y) const -> std::optional<WallPosition>;

    /**
     @note
     The behavior is undefined if *this does not contain an unbuilt wall.
     */
    auto firstUnbuilt() const -> Walls::value_type;

    bool isMissingLastWall() const;

    bool isUnsafeToBuildWallIn() const;

    bool isSafeToBuildWallIn() const;

    auto layout() const -> Rect;

    auto unbuiltWalls() const -> Walls;

    /**
     Heat determines the number of rooms the next player can complete
     if the current player builds a third wall in this room.
     */
    int heat() const;

    void set(int heat);

private:
    bool built(Side side) const;

    int distance(Side side, int x, int y) const;

    int wallsBuilt() const;

    Tile tile{};
    std::array<bool, 4> walls{};
    /**
     @note
     The default value is 1 because that is the minimum number of
     rooms that can be completed if the room has three walls (obviously).
     */
    int roomsCompletableIfThirdWallIsBuilt = 1;
};

} // namespace rooms::gameboard

#endif
