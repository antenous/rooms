// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_GAMEBOARD_ROOM_HPP_
#define ROOMS_GAMEBOARD_ROOM_HPP_

#include "gameboard/GameboardError.hpp"
#include "gameboard/Tile.hpp"
#include "gameboard/WallBuiltEvent.hpp"
#include <array>

namespace rooms::gameboard
{

class Wall;

class Room
{
public:
    using Sides = Tile::Sides;

    using Walls = std::array<std::shared_ptr<Wall>, 4>;

    class CompletedByNonPlayer;

    explicit Room(const Rect& layout);

    auto asTile() const -> Tile;

    void link(const Room& linked, Side side);

    auto build(Side side) -> std::vector<Point>;

    bool completed() const;

    bool contains(int x, int y) const;

private:
    auto built() const -> Sides;

    bool built(Side side) const;

    int distance(Side side, int x, int y) const;

    auto nearestUnbuiltWall(int x, int y) const -> Side;

    auto position() const -> Point;

    auto wall(Side side) const -> Walls::value_type;

    Rect layout;
    Walls walls;

    WallBuiltEvent wallBuilt{};
};

class Room::CompletedByNonPlayer : public GameboardError
{
public:
    CompletedByNonPlayer();
};

} // namespace rooms::gameboard

#endif
