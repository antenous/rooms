// Copyright (c) 2022 Antero Nousiainen

#include "gameboard/GameboardView.hpp"
#include "gameboard/RoomCreatedEvent.hpp"
#include "gameboard/WallBuiltEvent.hpp"
#include "RoomView.hpp"
#include <common/Utility.hpp>
#include <signals/ScopedConnection.hpp>
#include <array>
#include <map>
#include <ranges>
#include <set>

namespace rooms::gameboard
{
namespace
{
auto positionBehind(const WallPosition& wall) -> Point
{
    switch (wall.side)
    {
    case Side::top:
        return wall.position - Point{0, 1};

    case Side::left:
        return wall.position - Point{1, 0};

    case Side::bottom:
        return wall.position + Point{0, 1};

    case Side::right:
        return wall.position + Point{1, 0};

    default: // LCOV_EXCL_LINE
        rooms::unreachable(); // LCOV_EXCL_LINE
    }
}

auto fromBack(const WallPosition& wall) -> WallPosition
{
    switch (wall.side)
    {
    case Side::top:
        return {positionBehind(wall), Side::bottom};

    case Side::left:
        return {positionBehind(wall), Side::right};

    case Side::bottom:
        return {positionBehind(wall), Side::top};

    case Side::right:
        return {positionBehind(wall), Side::left};

    default: // LCOV_EXCL_LINE
        rooms::unreachable(); // LCOV_EXCL_LINE
    }
}

template<typename Head, typename Tail>
void append(Head& head, const Tail& tail)
{
#ifdef __cpp_lib_containers_ranges
    // Since C++23
    head.append_range(tail);
#else
    head.insert(std::end(head), std::begin(tail), std::end(tail));
#endif
}

using Rooms = std::map<Point, RoomView, PointCmp>;

class Completable
{
public:
    using Points = std::vector<Point>;

    explicit Completable(Rooms rooms);

    /**
     Returns the last wall that can be built on the next turn and all completable rooms
     if the specified wall is built

     @note
     The order of the completable rooms matter; heat should not be updated beyond "safe" rooms.
     */
    auto ifBuilt(const WallPosition& wall) -> std::pair<WallPosition, Points>;

private:
    auto build(const WallPosition& wall) -> WallPosition;

    auto buildSide(const WallPosition& wall) -> WallPosition;

    auto completeRooms(std::optional<WallPosition> wall) -> std::optional<WallPosition>;

    bool isMissingLastWall(Point position) const;

    auto lastWallIn(Point position) const -> std::optional<WallPosition>;

    Rooms rooms;
    Points completable;
};
} // namespace

class GameboardView::GameboardViewImpl
{
public:
    GameboardViewImpl();

    GameboardViewImpl(const GameboardViewImpl& other);

    Rect layout(Point position) const;

    auto nearestUnbuiltWall(int x, int y) const -> std::optional<WallPosition>;

    Walls lastWalls() const;

    Walls safeWalls() const;

    Walls unsafeWalls() const;

    void monitorHeat();

    auto heatmap() const -> Heatmap;

    auto heatDelta() const -> HeatDelta;

private:
    /**
     Container for heat points, from which the heat "spreads" to the adjacent rooms.
     Defined as a set to ensure there are no duplicates.
     */
    using Foci = std::set<Point, PointCmp>;

    auto filter(std::function<bool(const RoomView&)> fn) const -> Rooms;

    /**
     Initialize the heatmap, must be invoked before heat monitoring can be started.
     */
    void initHeatmap();

    void onWallBuilt(const WallPosition& wall);

    /**
     Update the room heats (i.e. the heatmap) when a wall is built.
     */
    void refreshHeatmap(const WallPosition& wall);

    /**
     Measure and update the heat starting from the foci and
     return the points from where the heat "reflects" back.
     */
    auto measureAndUpdateHeat(const Foci& foci) -> Foci;

    void updateHeat(const Completable::Points& completable);

    void updateHeat(Point position, int heat);

    /**
     Adjusts heat if all neighboring rooms are missing only one wall.
     */
    int adjustBasedOnNeighbors(Point position, int heat) const;

    /**
     An unsafe room with only one wall missing from both neighboring rooms is essentially a
     "safe" room, meaning building a wall in this room will only complete one of the other
     rooms, allowing the current player to continue playing.
     */
    bool isMadeSafeByNeighbors(Point position) const;

    bool isSafeToBuildWallIn(Point position) const;

    void createSafeWallsCache() const;

    auto markAsBuilt(const WallPosition& wall) -> Walls;

    auto markOneSideAsBuilt(Point position, Side side) -> Walls;

    void removeFromSafeWallsCache(const Walls& walls);

    auto wallsMadeUnsafe(const RoomView& room) -> Walls;

    signals::ScopedConnection roomCreatedSubscription;
    signals::ScopedConnection wallBuiltSubscription;
    Rooms rooms;
    bool monitoringHeat = false;
    HeatDelta delta;
    mutable Walls safeWallsCache;
};

GameboardView::GameboardView() :
    pimpl(std::make_unique<GameboardViewImpl>())
{
}

GameboardView::GameboardView(GameboardView&&) = default; // LCOV_EXCL_LINE

GameboardView::~GameboardView() = default;

GameboardView& GameboardView::operator=(GameboardView&&) = default; // LCOV_EXCL_LINE

GameboardView::GameboardViewImpl::GameboardViewImpl()
{
    roomCreatedSubscription = RoomCreatedEvent::subscribe([this](const Tile& tile) {
        rooms.emplace(tile.position, tile);
    });

    wallBuiltSubscription = WallBuiltEvent::subscribe([this](const WallPosition& wall) {
        onWallBuilt(wall);
    });
}

void GameboardView::GameboardViewImpl::onWallBuilt(const WallPosition& wall)
{
    const auto& wallsBuiltOrMadeUnsafe = markAsBuilt(wall);
    removeFromSafeWallsCache(wallsBuiltOrMadeUnsafe);
    refreshHeatmap(wall);
}

auto GameboardView::GameboardViewImpl::markAsBuilt(const WallPosition& wall) -> Walls
{
    auto wallsBuiltOrMadeUnsafe = Walls{};
    wallsBuiltOrMadeUnsafe.reserve(10);

    for (auto [position, side] : {wall, fromBack(wall)})
        append(wallsBuiltOrMadeUnsafe, markOneSideAsBuilt(position, side));

    std::sort(std::begin(wallsBuiltOrMadeUnsafe), std::end(wallsBuiltOrMadeUnsafe));
    return wallsBuiltOrMadeUnsafe;
}

auto GameboardView::GameboardViewImpl::markOneSideAsBuilt(Point position, Side side) -> Walls
{
    auto& room = rooms.at(position);
    room.build(side);

    auto wallsBuiltOrMadeUnsafe = Walls{{position, side}};
    append(wallsBuiltOrMadeUnsafe, wallsMadeUnsafe(room));
    return wallsBuiltOrMadeUnsafe;
}

auto GameboardView::GameboardViewImpl::wallsMadeUnsafe(const RoomView& room) -> Walls
{
    if (room.isSafeToBuildWallIn())
        return {};

    auto unsafe = Walls{};
    const auto& unbuilt = room.unbuiltWalls();
    append(unsafe, unbuilt);
    append(unsafe, unbuilt | std::views::transform(fromBack));
    return unsafe;
}

void GameboardView::GameboardViewImpl::removeFromSafeWallsCache(const Walls& walls)
{
    // Custom inline set_difference
    // https://en.cppreference.com/w/cpp/algorithm/set_difference
    std::erase_if(
        safeWallsCache,
        [it = std::begin(walls), last = std::end(walls)](auto& wall) mutable -> bool {
            while (it != last && *it < wall)
                ++it;
            return (it != last && *it == wall);
        });
}

auto GameboardView::nearestUnbuiltWall(int x, int y) const -> std::optional<WallPosition>
{
    return pimpl->nearestUnbuiltWall(x, y);
}

auto GameboardView::GameboardViewImpl::nearestUnbuiltWall(int x, int y) const
    -> std::optional<WallPosition>
{
    const auto it = std::ranges::find_if(rooms, [x, y](const auto& pair) {
        return pair.second.contains(x, y);
    });

    if (it == std::end(rooms))
        return std::nullopt;

    return it->second.nearestUnbuiltWall(x, y);
}

auto GameboardView::lastWalls() const -> Walls
{
    return pimpl->lastWalls();
}

auto GameboardView::GameboardViewImpl::lastWalls() const -> Walls
{
    Walls walls;

    for (auto& [position, room] : filter(&RoomView::isMissingLastWall))
        walls.emplace_back(room.firstUnbuilt());

    return walls;
}

auto GameboardView::safeWalls() const -> Walls
{
    return pimpl->safeWalls();
}

auto GameboardView::GameboardViewImpl::safeWalls() const -> Walls
{
    if (!safeWallsCache.empty())
        return safeWallsCache;

    createSafeWallsCache();
    return safeWallsCache;
}

void GameboardView::GameboardViewImpl::createSafeWallsCache() const
{
    for (auto& [position, room] : filter(&RoomView::isSafeToBuildWallIn))
        for (auto wall : room.unbuiltWalls())
            if (isSafeToBuildWallIn(positionBehind(wall)))
                safeWallsCache.emplace_back(wall);

    // Walls should already be sorted, but just in case they aren't...
    std::sort(std::begin(safeWallsCache), std::end(safeWallsCache));
}

auto GameboardView::unsafeWalls() const -> Walls
{
    pimpl->monitorHeat();
    return pimpl->unsafeWalls();
}

auto GameboardView::GameboardViewImpl::unsafeWalls() const -> Walls
{
    std::map<int, Walls> walls;

    for (auto& [position, room] : filter(&RoomView::isUnsafeToBuildWallIn))
        std::ranges::copy(room.unbuiltWalls(), std::back_inserter(walls[room.heat()]));

    return walls.begin()->second;
}

auto GameboardView::GameboardViewImpl::filter(std::function<bool(const RoomView&)> fn) const
    -> Rooms
{
    Rooms result;
    std::ranges::copy_if(rooms, std::inserter(result, std::end(result)), [&fn](auto& pair) {
        return std::invoke(fn, pair.second);
    });
    return result;
}

void GameboardView::GameboardViewImpl::monitorHeat()
{
    if (std::exchange(monitoringHeat, true))
        return;

    initHeatmap();
}

void GameboardView::GameboardViewImpl::initHeatmap()
{
    // NOTE: To speed up the creation of the initial heatmap,
    // only rooms with default heat (1) are updated.
    constexpr auto defaultHeat = 1;

    std::ranges::for_each(rooms | std::views::values, [&](const RoomView& room) {
        if (room.heat() != defaultHeat)
            return;

        updateHeat(
            std::get<Completable::Points>(Completable{rooms}.ifBuilt(room.firstUnbuilt())));
    });
}

void GameboardView::GameboardViewImpl::refreshHeatmap(const WallPosition& wall)
{
    if (!monitoringHeat)
        return;

    delta.clear();
    measureAndUpdateHeat(measureAndUpdateHeat({wall.position, positionBehind(wall)}));
}

auto GameboardView::GameboardViewImpl::measureAndUpdateHeat(const Foci& foci) -> Foci
{
    auto reflectionPoints = Foci{};

    std::ranges::for_each(foci, [this, &reflectionPoints](Point position) {
        std::ranges::for_each(
            rooms.at(position).unbuiltWalls(),
            [this, &reflectionPoints](const WallPosition& wall) {
                const auto& [lastWall, completable] = Completable{rooms}.ifBuilt(wall);
                updateHeat(completable);
                reflectionPoints.insert(lastWall.position);
            });
    });

    return reflectionPoints;
}

void GameboardView::GameboardViewImpl::updateHeat(const Completable::Points& completable)
{
    for (auto heat = std::ssize(completable); auto position : completable)
        if (isSafeToBuildWallIn(position))
            break;
        else
            updateHeat(position, heat);
}

bool GameboardView::GameboardViewImpl::isSafeToBuildWallIn(Point position) const
{
    return rooms.at(position).isSafeToBuildWallIn();
}

void GameboardView::GameboardViewImpl::updateHeat(Point position, int heat)
{
    heat = adjustBasedOnNeighbors(position, heat);
    auto& room = rooms.at(position);

    if (room.heat() == heat)
        return;

    room.set(heat);
    delta.emplace_back(position, room.heat());
}

int GameboardView::GameboardViewImpl::adjustBasedOnNeighbors(Point position, int heat) const
{
    return (heat == 3 && isMadeSafeByNeighbors(position)) ? 1 : heat;
}

bool GameboardView::GameboardViewImpl::isMadeSafeByNeighbors(Point position) const
{
    for (auto unbuilt : rooms.at(position).unbuiltWalls())
        if (!rooms.at(positionBehind(unbuilt)).isMissingLastWall())
            return false;

    return true;
}

Completable::Completable(Rooms rooms) :
    rooms(std::move(rooms))
{
}

auto Completable::ifBuilt(const WallPosition& wall) -> std::pair<WallPosition, Points>
{
    const auto lastBuilt = completeRooms(build(wall)).value();
    return {completeRooms(lastWallIn(wall.position)).value_or(lastBuilt), completable};
}

auto Completable::completeRooms(std::optional<WallPosition> wall)
    -> std::optional<WallPosition>
{
    auto lastBuilt = wall;

    while (wall.has_value() && isMissingLastWall(wall->position))
        if (wall = build(*lastWallIn(wall->position)); wall)
            lastBuilt = wall;

    return lastBuilt;
}

auto Completable::lastWallIn(Point position) const -> std::optional<WallPosition>
{
    if (!isMissingLastWall(position))
        return std::nullopt;

    return rooms.at(position).firstUnbuilt();
}

bool Completable::isMissingLastWall(Point position) const
{
    return rooms.at(position).isMissingLastWall();
}

auto Completable::build(const WallPosition& wall) -> WallPosition
{
    buildSide(wall);
    return buildSide(fromBack(wall));
}

auto Completable::buildSide(const WallPosition& wall) -> WallPosition
{
    auto& room = rooms.at(wall.position);

    if (room.isMissingLastWall())
        completable.push_back(wall.position);

    room.build(wall.side);
    return wall;
}

auto GameboardView::heatmap() const -> Heatmap
{
    pimpl->monitorHeat();
    return pimpl->heatmap();
}

auto GameboardView::GameboardViewImpl::heatmap() const -> Heatmap
{
    auto heatmap = Heatmap{};
    heatmap.reserve(std::size(rooms));

    std::ranges::transform(
        rooms | std::views::values, std::back_inserter(heatmap), std::mem_fn(&RoomView::heat));

    return heatmap;
}

Rect GameboardView::layout(Point position) const
{
    return pimpl->layout(position);
}

Rect GameboardView::GameboardViewImpl::layout(Point position) const
{
    return rooms.at(position).layout();
}

auto GameboardView::heatDelta() const -> HeatDelta
{
    pimpl->monitorHeat();
    return pimpl->heatDelta();
}

auto GameboardView::GameboardViewImpl::heatDelta() const -> HeatDelta
{
    return delta;
}

} // namespace rooms::gameboard
