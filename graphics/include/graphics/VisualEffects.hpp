// Copyright (c) 2023 Antero Nousiainen

#ifndef ROOMS_GRAPHICS_VISUALEFFECTS_HPP_
#define ROOMS_GRAPHICS_VISUALEFFECTS_HPP_

#include "Color.hpp"
#include <common/Rect.hpp>
#include <optional>
#include <vector>

namespace rooms::graphics
{

class VisualEffects
{
public:
    void addHighlight(const Rect& layout, Color color);

    void setSelected(const Rect& layout, Color color);

    void clear();

private:
    friend class Image;

    struct Effect
    {
#ifndef __cpp_aggregate_paren_init
        // NOTE: Not yet supported by Clang (https://clang.llvm.org/cxx_status.html)
        Effect(const Rect& layout, Color color) :
            layout(layout),
            color(color)
        {
        }
#endif
        Rect layout;
        Color color;
    };

    std::vector<Effect> highlights;
    std::optional<Effect> selected;
};

} // namespace rooms::graphics

#endif
