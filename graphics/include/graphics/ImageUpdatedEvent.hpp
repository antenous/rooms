// Copyright (c) 2020 Antero Nousiainen

#ifndef ROOMS_GRAPHICS_IMAGEUPDATEDEVENT_HPP_
#define ROOMS_GRAPHICS_IMAGEUPDATEDEVENT_HPP_

#include <signals/Event.hpp>
#include <QImage>

namespace rooms::graphics
{

struct ImageUpdatedEvent : signals::Event<ImageUpdatedEvent, void(const QImage&)>
{
};

} // namespace rooms::graphics

#endif
