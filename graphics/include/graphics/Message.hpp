// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_GRAPHICS_MESSAGE_HPP_
#define ROOMS_GRAPHICS_MESSAGE_HPP_

#include "Color.hpp"
#include <QImage>

namespace rooms::graphics
{

class Message
{
public:
    struct Score;

    Message();

    /**
     Add a (centered) row of text
     */
    Message& operator<<(std::string_view text);

    /**
     Add a score row
     */
    Message& operator<<(const Score& score);

    int height() const;

    int width() const;

private:
    friend class Image;

    /**
     Increases the size of the drawing area (if needed) and
     returns the area reserved for the operation
     */
    auto resizeToFit(int width, int height) -> QRect;

    int widen(int width) const;

    int heighten(int height) const;

    /**
     https://doc.qt.io/qt-5/qfontmetrics.html#leading
     */
    int leading = 2;

    QImage image;

    /**
     https://doc.qt.io/qt-6/qrect.html#rendering
     */
    int penWidth = 1;
};

struct Message::Score
{
    Color color;
    int score;
};

} // namespace rooms::graphics

#endif
