// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GRAPHICS_IMAGE_HPP_
#define ROOMS_GRAPHICS_IMAGE_HPP_

#include "Color.hpp"
#include "ImageUpdatedEvent.hpp"
#include <common/Rect.hpp>
#include <QImage>

namespace rooms::graphics
{

class Message;
class VisualEffects;

class Image
{
public:
    Image();

    Image(int width, int height);

    void drawEffects(const VisualEffects& effects);

    void drawHeat(const Rect& rect, int heat);

    void drawImage(int x, int y, const Image& other);

    void drawMessage(int x, int y, const Message& message);

    void drawRect(
        const Rect& rect, const Color& pen = Qt::black, const Color& brush = Qt::transparent);

    void fillRect(const Rect& rect, const Color& color);

    int height() const;

    void update() const;

    int width() const;

private:
    QImage image;
    ImageUpdatedEvent updated{};
};

} // namespace rooms::graphics

#endif
