// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GRAPHICS_COLOR_HPP_
#define ROOMS_GRAPHICS_COLOR_HPP_

#include <QColor>

namespace rooms::graphics
{

using Color = QColor;
using Rgb = QRgb;

} // namespace rooms::graphics

#endif
