// Copyright (c) 2021 Antero Nousiainen

#include <graphics/ImageUpdatedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::graphics
{
namespace
{
TEST(ImageUpdatedEventTest, SubscribeEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        ImageUpdatedEvent::subscribe([&subscriberInvoked](const QImage&) {
            subscriberInvoked = true;
        });

    std::invoke(ImageUpdatedEvent{}, QImage{});
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::graphics
