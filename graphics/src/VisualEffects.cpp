// Copyright (c) 2023 Antero Nousiainen

#include "graphics/VisualEffects.hpp"

namespace rooms::graphics
{
void VisualEffects::addHighlight(const Rect& layout, Color color)
{
    highlights.emplace_back(layout, color);
}

void VisualEffects::setSelected(const Rect& layout, Color color)
{
    selected = {layout, color};
}

void VisualEffects::clear()
{
    highlights.clear();
    selected.reset();
}
} // namespace rooms::graphics
