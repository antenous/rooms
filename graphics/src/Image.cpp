// Copyright (c) 2021 Antero Nousiainen

#include "graphics/Image.hpp"
#include "graphics/Message.hpp"
#include "graphics/VisualEffects.hpp"
#include <fmt/format.h>
#include <QPainter>

namespace rooms::graphics
{
Image::Image() :
    Image(0, 0)
{
}

Image::Image(int width, int height) :
    image{width, height, QImage::Format_ARGB32}
{
    image.fill(Qt::transparent);
}

void Image::drawEffects(const VisualEffects& effects)
{
    for (const auto& [layout, color] : effects.highlights)
        fillRect(layout, color);

    if (!effects.selected)
        return;

    drawRect(effects.selected->layout - QMargins{1, 1, 1, 1}, effects.selected->color);
}

void Image::drawHeat(const Rect& rect, int heat)
{
    const auto adjusted = rect.adjusted(2, 2, -2, -2);

    QPainter painter{&image};
    auto font = painter.font();
    font.setPointSize(6);
    painter.setFont(font);
    painter.setBrush(Qt::white);
    painter.setPen(Qt::darkGray);

    // Because the heat can change after each turn and drawing a full heatmap is quite a heavy
    // task, only the delta is drawn, which means the previous values must be cleared first.
    // https://doc.qt.io/qt-6/qpainter.html#CompositionMode-enum
    painter.setCompositionMode(QPainter::CompositionMode_Clear);
    painter.drawRect(adjusted);

    // The default heat is 1, so showing it feels pointless
    if (heat <= 1)
        return;

    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawText(
        adjusted, Qt::AlignTop | Qt::AlignLeft, std::data(fmt::format("{}", heat)));
}

void Image::drawImage(int x, int y, const Image& other)
{
    QPainter{&image}.drawImage(x, y, other.image);
}

void Image::drawMessage(int x, int y, const Message& message)
{
    QPainter{&image}.drawImage(x, y, message.image);
}

void Image::drawRect(const Rect& rect, const Color& pen, const Color& brush)
{
    QPainter painter{&image};
    painter.setBrush(brush);
    painter.setPen(pen);
    painter.drawRect(rect);
}

void Image::fillRect(const Rect& rect, const Color& color)
{
    drawRect(rect - QMargins{1, 1, 0, 0}, Qt::transparent, color);
}

int Image::height() const
{
    return image.height();
}

int Image::width() const
{
    return image.width();
}

void Image::update() const
{
    updated(image);
}
} // namespace rooms::graphics
