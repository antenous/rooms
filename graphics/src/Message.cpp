// Copyright (c) 2022 Antero Nousiainen

#include "graphics/Message.hpp"
#include <fmt/format.h>
#include <QPainter>

namespace rooms::graphics
{
namespace
{
auto areaSize(std::string_view text)
{
    return QFontMetrics{QFont{}}.boundingRect(std::data(text)).size();
}
} // namespace

Message::Message() :
    image{leading, leading, QImage::Format_RGB32}
{
    image.fill(Qt::white);
}

Message& Message::operator<<(const Score& score)
{
    const auto text = fmt::format(" × {}", score.score);
    const auto [width, height] = areaSize(text);
    const auto [x, y] = resizeToFit(width + height, height).topLeft();
    const auto tile = QRect{x, y, height, height};

    auto painter = QPainter{&image};
    painter.setBrush(score.color);
    painter.drawRect(tile);
    painter.drawText(x + tile.width(), y, width, height, Qt::AlignLeft, std::data(text));

    return *this;
}

Message& Message::operator<<(std::string_view text)
{
    const auto [width, height] = areaSize(text);
    const auto rect = resizeToFit(width, height);

    // TODO: Centering only works well as long as the first string is the longest
    QPainter{&image}.drawText(rect, Qt::AlignCenter, std::data(text));

    return *this;
}

auto Message::resizeToFit(int width, int height) -> QRect
{
    auto other = QImage{widen(width), heighten(height), QImage::Format_RGB32};
    other.fill(Qt::white);
    QPainter{&other}.drawImage(0, 0, image);
    std::swap(image, other);
    return {leading, other.height(), image.width() - leading * 2, height};
}

int Message::widen(int width) const
{
    return std::max(image.width(), width + leading * 2);
}

int Message::heighten(int height) const
{
    return image.height() + height + leading + penWidth;
}

int Message::height() const
{
    return image.height();
}

int Message::width() const
{
    return image.width();
}
} // namespace rooms::graphics
