// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_WARGAMES_HPP_
#define ROOMS_WARGAMES_HPP_

#include <core/PlayerFactory.hpp>

namespace rooms
{

class WarGames : public core::PlayerFactory
{
public:
    Players createPlayers() const override;
};

} // namespace rooms

#endif
