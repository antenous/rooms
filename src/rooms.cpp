// Copyright (c) 2021 Antero Nousiainen

#include "config.hpp"
#include "BoardWidget.hpp"
#include "GameLauncher.hpp"
#include "MainWindow.hpp"
#include "SinglePlayer.hpp"
#include "WarGames.hpp"
#include <core/Context.hpp>
#include <player/MouseMovedEvent.hpp>
#include <fmt/format.h>
#include <QApplication>
#include <QCommandLineParser>
#include <QMessageBox>
#include <QShortcut>
#include <QTimer>

namespace
{
const int HEIGHT = 20;
const int WIDTH = 20;
constexpr int SMALL = 7;
//    constexpr int MEDIUM = 13;
//    constexpr int LARGE = 19;
constexpr std::string_view gamesave{"save.txt"};

class Context : public rooms::core::Context
{
public:
    explicit Context(QObject& context) :
        context(context),
        timer(&context)
    {
        gameStartedSubscription = rooms::core::GameStartedEvent::subscribe([this] {
            using namespace std::chrono_literals;
            QObject::connect(&timer, &QTimer::timeout, [this]() {
                tick();
            });
            timer.start(10ms);
        });

        gameEndedSubscription = rooms::core::GameEndedEvent::subscribe([this](auto&) {
            timer.stop();
        });
    }

    void defer(Callable callable) override
    {
        deferred = std::move(callable);
    }

    void post(Callable callable) override
    {
        using namespace std::chrono_literals;
        QTimer::singleShot(0ms, &context, std::move(callable));
    }

private:
    void tick()
    {
        if (!deferred)
            return;

        std::invoke(std::exchange(deferred, nullptr));
    }

    const QObject& context;
    QTimer timer;
    Callable deferred;
    signals::ScopedConnection gameStartedSubscription;
    signals::ScopedConnection gameEndedSubscription;
};
} // namespace

int main(int argc, char* argv[])
{
    QApplication app{argc, argv};
    QApplication::setApplicationName(rooms::PROJECT_NAME.data());
    QApplication::setApplicationVersion(rooms::PROJECT_VERSION.data());

    QCommandLineParser parser;
    parser.setApplicationDescription("A game of rooms");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption benchmarkOption(
        "benchmark", "Run the benchmark on a gameboard of size 7–49.", "size");
    parser.addOption(benchmarkOption);
    parser.process(app);

    rooms::MainWindow mainWindow;
    rooms::BoardWidget board;
    mainWindow.setCentralWidget(&board);
    mainWindow.show();

    Context context{app};
    rooms::GameLauncher launcher{context, WIDTH, HEIGHT};

    QObject::connect(mainWindow.actionNew, &QAction::triggered, [&launcher](bool) {
        launcher.newGame(SMALL, rooms::SinglePlayer{});
    });

    QObject::connect(mainWindow.actionQuick, &QAction::triggered, [&launcher](bool) {
        launcher.quickGame(SMALL, rooms::SinglePlayer{});
    });

    QShortcut warGames{QKeySequence{QKeyCombination{Qt::CTRL, Qt::Key_W}}, &board};
    QObject::connect(&warGames, &QShortcut::activated, [&launcher] {
        launcher.newGame(SMALL, rooms::WarGames{});
    });

#ifndef NDEBUG
    QShortcut heatmap{QKeySequence{QKeyCombination{Qt::CTRL | Qt::SHIFT, Qt::Key_H}}, &board};
    QObject::connect(&heatmap, &QShortcut::activated, [&launcher] {
        launcher.toggleHeatmap();
    });
#endif

    QObject::connect(mainWindow.actionAbout, &QAction::triggered, [&mainWindow](bool) {
        // https://doc.qt.io/archives/qt-5.12/qt-sub-qtgui.html#mightBeRichText
        // "It mainly checks whether there is something that looks like a tag before the first line break."
        QMessageBox::about(
            &mainWindow, fmt::format("About {}", rooms::PROJECT_NAME).c_str(),
            fmt::format(
                "Version {0} (Qt {2})<br>"
                "Copyright © 2005 - 2025 Antero Nousiainen<br>"
                "<a href='{1}'>{1}</a>",
                rooms::PROJECT_VERSION, rooms::PROJECT_HOMEPAGE_URL, QT_VERSION_STR)
                .c_str());
    });

    rooms::player::MouseMovedEvent::subscribe(
        [statusBar = mainWindow.statusBar()](int x, int y) {
            statusBar->showMessage(fmt::format("({},{})", x, y).c_str());
        });

    if (parser.isSet(benchmarkOption))
    {
        mainWindow.statusBar()->showMessage("Benchmarking");
        mainWindow.setEnabled(false);

        launcher.newGame(
            std::clamp(parser.value(benchmarkOption).toInt(), 7, 49), rooms::WarGames{});

        rooms::core::GameEndedEvent::subscribe([&app](auto&) {
            app.quit();
        });
    }
    else
    {
        try
        {
            launcher.load(gamesave, rooms::SinglePlayer{});
        }
        catch (const rooms::core::CoreError& e)
        {
            mainWindow.statusBar()->showMessage(e.what());
            launcher.newGame(SMALL, rooms::SinglePlayer{});
        }

        QObject::connect(&app, &QCoreApplication::aboutToQuit, [&launcher] {
            launcher.save(gamesave);
        });
    }

    return app.exec();
}
