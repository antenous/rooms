// Copyright (c) 2021 Antero Nousiainen

#include "GameLauncher.hpp"
#include <common/Random.hpp>
#include <core/GameLoader.hpp>
#include <core/QuickGame.hpp>
#include <fstream>

namespace rooms
{
GameLauncher::GameLauncher(core::Context& context, int width, int height) :
    context(context),
    width(width),
    height(height)
{
}

void GameLauncher::load(
    const std::filesystem::path& gamesave, const core::PlayerFactory& playerFactory)
{
    auto file = std::ifstream{gamesave};
    auto loader = core::GameLoader{};
    loader.load(file);

    createGame(loader.size(), playerFactory);
    loader.restore(*game);
    game->start();
}

void GameLauncher::createGame(int size, const core::PlayerFactory& playerFactory)
{
    const auto dimensions = Dimensions{width, height, size};
    gamePainter = std::make_unique<GamePainter>(dimensions);
    game = std::make_unique<core::Game>(context, playerFactory, dimensions);
}

void GameLauncher::newGame(int size, const core::PlayerFactory& playerFactory)
{
    createGame(size, playerFactory);
    game->start();
}

void GameLauncher::quickGame(int size, const core::PlayerFactory& playerFactory)
{
    core::QuickGame quickGame;
    createGame(size, playerFactory);
    const auto wallsLeftUnbuilt = random(size);
    quickGame.setUp(*game, wallsLeftUnbuilt);
    gamePainter->clear();
    game->start();
}

void GameLauncher::toggleHeatmap()
{
    gamePainter->toggleHeatmap();
}

void GameLauncher::save(const std::filesystem::path& gamesave) const
{
    try
    {
        if (std::ofstream file(gamesave); file)
            game->save(file);
    }
    catch (const core::CoreError&)
    {
        std::filesystem::remove(gamesave);
    }
}
} // namespace rooms
