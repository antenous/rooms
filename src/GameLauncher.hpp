// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GAMELAUNCHER_HPP_
#define ROOMS_GAMELAUNCHER_HPP_

#include "GamePainter.hpp"
#include <core/Game.hpp>
#include <filesystem>

namespace rooms
{

class GameLauncher
{
public:
    GameLauncher(core::Context& context, int width, int height);

    void load(const std::filesystem::path& gamesave, const core::PlayerFactory& playerFactory);

    void newGame(int size, const core::PlayerFactory& playerFactory);

    void quickGame(int size, const core::PlayerFactory& playerFactory);

    void save(const std::filesystem::path& gamesave) const;

    void toggleHeatmap();

private:
    void createGame(int size, const core::PlayerFactory& playerFactory);

    core::Context& context;
    int width;
    int height;
    std::unique_ptr<GamePainter> gamePainter;
    std::unique_ptr<core::Game> game;
};

} // namespace rooms

#endif
