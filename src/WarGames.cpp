// Copyright (c) 2021 Antero Nousiainen

#include "WarGames.hpp"
#include <player/ComputerPlayer.hpp>

namespace rooms
{
auto WarGames::createPlayers() const -> Players
{
    using core::PlayerId;
    return {
        std::make_unique<player::ComputerPlayer>(PlayerId::p1, "Max"),
        std::make_unique<player::ComputerPlayer>(PlayerId::p2, "Lola")};
}
} // namespace rooms
