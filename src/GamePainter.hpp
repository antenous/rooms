// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_GAMEPAINTER_HPP_
#define ROOMS_GAMEPAINTER_HPP_

#include <common/Dimensions.hpp>
#include <core/GameEndedEvent.hpp>
#include <gameboard/GameboardView.hpp>
#include <graphics/Image.hpp>
#include <graphics/Message.hpp>
#include <graphics/VisualEffects.hpp>
#include <signals/ScopedConnection.hpp>

namespace rooms
{

namespace gameboard
{
struct Tile;
}

class GamePainter
{
public:
    explicit GamePainter(const Dimensions& dimensions);

    void toggleHeatmap();

    void clear();

private:
    auto createGameOverMessage(const core::Scorecards& scorecards) const -> graphics::Message;

    void drawBuiltWalls(const gameboard::Tile& tile);

    void drawHeatmap();

    void refreshHeatmap();

    void update() const;

    Dimensions dimensions;

    graphics::Image background;
    graphics::Image foreground;
    graphics::VisualEffects effects;

    bool gameStarted = false;
    bool showHeat = false;

    gameboard::GameboardView gameboard;
    signals::ScopedConnection gameStartedSubscription;
    signals::ScopedConnection gameEndedSubscription;
    signals::ScopedConnection roomCompletedSubscription;
    signals::ScopedConnection roomCreatedSubscription;
    signals::ScopedConnection wallBuiltSubscription;
    signals::ScopedConnection wallSelectedSubscription;
};

} // namespace rooms

#endif
