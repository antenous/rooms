// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_COLORS_HPP_
#define ROOMS_COLORS_HPP_

#include <graphics/Color.hpp>

namespace rooms::colors
{
namespace player
{
constexpr graphics::Rgb p1 = 0x5d93df;
constexpr graphics::Rgb p2 = 0xde73b6;
} // namespace player

namespace room
{
constexpr graphics::Rgb highlighted = 0xf0f0f0;
constexpr graphics::Rgb none = 0xffffff;
} // namespace room

namespace wall
{
constexpr graphics::Rgb built = 0x0;
constexpr graphics::Rgb none = 0xf0f0f0;
constexpr graphics::Rgb selected = 0xff0000;
} // namespace wall
} // namespace rooms::colors

#endif
