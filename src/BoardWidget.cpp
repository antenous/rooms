// Copyright (c) 2021 Antero Nousiainen

#include "BoardWidget.hpp"
#include <graphics/ImageUpdatedEvent.hpp>
#include <QMouseEvent>
#include <QPainter>

namespace rooms
{
BoardWidget::BoardWidget(QWidget* parent) :
    QWidget(parent)
{
    setMouseTracking(true);

    updateSubscription = graphics::ImageUpdatedEvent::subscribe([this](auto&& image) {
        this->image = image;
        update();
    });
}

void BoardWidget::mouseMoveEvent(QMouseEvent* event)
{
    mouseMoved(event->pos().x(), event->pos().y());
}

void BoardWidget::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton)
        mouseReleased(event->pos().x(), event->pos().y());
}

void BoardWidget::paintEvent(QPaintEvent*)
{
    resize();
    QPainter{this}.drawImage(0, 0, image);
}

void BoardWidget::resize()
{
    if (size() == image.size())
        return;

    setFixedSize(image.size());
    parentWidget()->setFixedSize(parentWidget()->sizeHint());
}
} // namespace rooms
