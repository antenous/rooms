// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_MAINWINDOW_HPP_
#define ROOMS_MAINWINDOW_HPP_

#ifndef QT_NO_KEYWORDS
#define QT_NO_KEYWORDS
#endif

#include <QMainWindow>
#include "ui_MainWindow.h"

namespace rooms
{

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);

private Q_SLOTS:
    void on_actionQuit_triggered();

private:
    void disableMaximizeButton();
};

} // namespace rooms

#endif
