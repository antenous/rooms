// Copyright (c) 2021 Antero Nousiainen

#include "SinglePlayer.hpp"
#include <player/ComputerPlayer.hpp>
#include <player/LocalPlayer.hpp>

namespace rooms
{
auto SinglePlayer::createPlayers() const -> Players
{
    using core::PlayerId;
    return {
        std::make_unique<player::LocalPlayer>(PlayerId::p1, "You"),
        std::make_unique<player::ComputerPlayer>(PlayerId::p2, "I")};
}
} // namespace rooms
