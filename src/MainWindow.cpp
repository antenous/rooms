// Copyright (c) 2021 Antero Nousiainen

#include "MainWindow.hpp"

namespace rooms
{
MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent)
{
    disableMaximizeButton();
    setupUi(this);
}

void MainWindow::disableMaximizeButton()
{
    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint);
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}
} // namespace rooms
