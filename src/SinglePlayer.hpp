// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_SINGLEPLAYER_HPP_
#define ROOMS_SINGLEPLAYER_HPP_

#include <core/PlayerFactory.hpp>

namespace rooms
{

class SinglePlayer : public core::PlayerFactory
{
public:
    Players createPlayers() const override;
};

} // namespace rooms

#endif
