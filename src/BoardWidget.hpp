// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_BOARDWIDGET_HPP_
#define ROOMS_BOARDWIDGET_HPP_

#include <player/MouseMovedEvent.hpp>
#include <player/MouseReleasedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <QWidget>

namespace rooms
{

class BoardWidget : public QWidget
{
public:
    explicit BoardWidget(QWidget* parent = nullptr);

private:
    void mouseMoveEvent(QMouseEvent* event) override;

    void mouseReleaseEvent(QMouseEvent* event) override;

    void paintEvent(QPaintEvent* event) override;

    void resize();

    QImage image;
    player::MouseMovedEvent mouseMoved{};
    player::MouseReleasedEvent mouseReleased{};
    signals::ScopedConnection updateSubscription;
};

} // namespace rooms

#endif
