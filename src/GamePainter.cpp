// Copyright (c) 2021 Antero Nousiainen

#include "GamePainter.hpp"
#include "Colors.hpp"
#include <core/GameStartedEvent.hpp>
#include <core/RoomCompletedEvent.hpp>
#include <gameboard/RoomCreatedEvent.hpp>
#include <gameboard/WallBuiltEvent.hpp>
#include <player/WallSelectedEvent.hpp>
#include <fmt/format.h>
#include <ranges>

namespace rooms
{
using namespace gameboard;
using namespace graphics;
using player::WallSelectedEvent;

namespace
{
void fadeToBlack(Image& image, const Dimensions& dimensions)
{
    const auto [width, height, size] = dimensions;

    for (int y = 0; y < size; ++y)
        for (int x = 0; x < size; ++x)
            image.drawRect(
                {x * width, y * height, width, height}, colors::wall::built,
                Color{0, 0, 0, 250 * (x + y) / (size * 2)});
}

auto color(core::PlayerId id) -> Color
{
    // This looks a bit suspicious because there are three player IDs,
    // but in practice this function is only ever called by two of them.
    return id == core::PlayerId::p1 ? colors::player::p1 : colors::player::p2;
}

auto color(const core::Scorecard& scorecard) -> Color
{
    return color(scorecard.id);
}

#ifdef NDEBUG
auto drawScoreBar(const core::Scorecards& scorecards) -> Image
{
    const auto total = std::accumulate(
        std::begin(scorecards), std::end(scorecards), 0,
        [](int sum, const core::Scorecard& scorecard) {
            return sum + scorecard.points;
        });
    const auto width = 30;
    auto scoreBar = Image{width + 1, total + 1};

    std::ranges::for_each(
        scorecards | std::views::reverse,
        [&scoreBar, height = 0](const core::Scorecard& scorecard) mutable {
            scoreBar.drawRect(
                Rect{0, height, width, scorecard.points}, colors::wall::built,
                color(scorecard));
            height += scorecard.points;
        });

    return scoreBar;
}
#endif
} // namespace

GamePainter::GamePainter(const Dimensions& dimensions) :
    dimensions(dimensions),
    background(
        dimensions.width * dimensions.size + 1, dimensions.height * dimensions.size + 1),
    foreground(background.width(), background.height())
{
    gameStartedSubscription = core::GameStartedEvent::subscribe([this] {
        gameStarted = true;
        update();
    });

    gameEndedSubscription = core::GameEndedEvent::subscribe(
        [this, dimensions](const core::Scorecards& scorecards) {
            fadeToBlack(background, dimensions);
#ifdef NDEBUG
            // Alternative scoring at the end of the game in release versions
            const auto scoreBar = drawScoreBar(scorecards);
            background.drawImage(
                (background.width() - scoreBar.width()) / 2,
                (background.height() - scoreBar.height()) / 2, scoreBar);
#else
            const auto message = createGameOverMessage(scorecards);
            background.drawMessage(
                (background.width() - message.width()) / 2,
                (background.height() - message.height()) / 2, message);
#endif
            background.update();
        });

    roomCreatedSubscription = RoomCreatedEvent::subscribe([this](const Tile& tile) {
        // NOTE: "Room created" events are created during the game setup phase,
        // i.e. the final image is ready only after the gameboard is fully created.
        background.drawRect(tile.layout, colors::wall::none, colors::room::none);
        drawBuiltWalls(tile);
    });

    roomCompletedSubscription =
        core::RoomCompletedEvent::subscribe([this](Point position, core::PlayerId id) {
            const auto& layout = gameboard.layout(position);
            background.fillRect(layout, color(id));
            effects.addHighlight(layout, color(id).lighter());
            update();
        });

    wallBuiltSubscription = WallBuiltEvent::subscribe([this](const WallPosition& wall) {
        const auto [position, side] = wall;
        const auto layout = gameboard.layout(position);

        background.drawRect(layout | extract(side), colors::wall::built);

        effects.clear();
        effects.addHighlight(layout, colors::room::highlighted);

        refreshHeatmap();
        update();
    });

    wallSelectedSubscription = WallSelectedEvent::subscribe([this](const WallPosition& wall) {
        const auto [position, side] = wall;
        effects.setSelected(
            gameboard.layout(position) | extract(side), colors::wall::selected);
        update();
    });
}

void GamePainter::refreshHeatmap()
{
    if (!gameStarted || !showHeat)
        return;

    const auto [width, height, size] = dimensions;

    for (auto& [position, heat] : gameboard.heatDelta())
        foreground.drawHeat(
            {position.x() * width, position.y() * height, width, height}, heat);
}

void GamePainter::update() const
{
    if (!gameStarted)
        return;

    auto finalImage = background;
    finalImage.drawEffects(effects);

    if (showHeat)
        finalImage.drawImage(0, 0, foreground);

    finalImage.update();
}

auto GamePainter::createGameOverMessage(const core::Scorecards& scorecards) const -> Message
{
    auto message = Message{};

    if (gameStarted)
        message << "Game Over" << fmt::format("{} won!", scorecards.front().name);

    for (auto& scorecard : scorecards)
        message << Message::Score{color(scorecard), scorecard.points};

    return message;
}

void GamePainter::drawBuiltWalls(const Tile& tile)
{
    std::ranges::for_each(tile.built, [this, layout = tile.layout](Side side) {
        background.drawRect(layout | extract(side), colors::wall::built);
    });
}

void GamePainter::toggleHeatmap()
{
    if (!gameStarted)
        return;

    showHeat ^= true;

    drawHeatmap();
    update();
}

void GamePainter::drawHeatmap()
{
    if (!showHeat)
        return;

    const auto [width, height, size] = dimensions;
    const auto heats = gameboard.heatmap();

    for (int y = 0, i = 0; y < size; ++y)
        for (int x = 0; x < size; ++x, ++i)
            foreground.drawHeat({x * width, y * height, width, height}, heats[i]);
}

void GamePainter::clear()
{
    effects.clear();
}

} // namespace rooms
