// Copyright (c) 2005 Antero Nousiainen

#include "player/ComputerPlayer.hpp"
#include <common/Random.hpp>
#include <functional>

namespace rooms::player
{
void ComputerPlayer::play(Turn&& turn)
{
    const auto views = {
        std::mem_fn(&gameboard::GameboardView::lastWalls),
        std::mem_fn(&gameboard::GameboardView::safeWalls),
        std::mem_fn(&gameboard::GameboardView::unsafeWalls)};

    for (auto& view : views)
        if (auto walls = std::invoke(view, gameboard); !walls.empty())
            return randomize(turn, walls);
}

void ComputerPlayer::randomize(Turn& turn, const Walls& walls)
{
    turn.set_value(walls[random(walls.size() - 1)]);
}
} // namespace rooms::player
