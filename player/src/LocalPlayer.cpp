// Copyright (c) 2005 Antero Nousiainen

#include "player/LocalPlayer.hpp"
#include "player/MouseMovedEvent.hpp"
#include "player/MouseReleasedEvent.hpp"

namespace rooms::player
{
void LocalPlayer::play(Turn&& turn)
{
    myTurn = std::move(turn);

    mouseMovedSubscription = MouseMovedEvent::subscribe([this](int x, int y) {
        try
        {
            const auto wall = gameboard.nearestUnbuiltWall(x, y).value();
            wallSelected(wall);
            mouseReleasedSubscription = MouseReleasedEvent::subscribe([this, wall](int, int) {
                myTurn.set_value(wall);
                mouseMovedSubscription.disconnect();
                mouseReleasedSubscription.disconnect();
            });
        }
        catch (const std::bad_optional_access&)
        {
        }
    });
}
} // namespace rooms::player
