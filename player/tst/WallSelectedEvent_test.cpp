// Copyright (c) 2021 Antero Nousiainen

#include <player/WallSelectedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::player
{
namespace
{
TEST(WallSelectedEventTest, SubscribeEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        WallSelectedEvent::subscribe([&subscriberInvoked](const WallPosition&) {
            subscriberInvoked = true;
        });

    std::invoke(WallSelectedEvent{}, WallPosition{});
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::player
