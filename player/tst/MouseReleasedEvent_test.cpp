// Copyright (c) 2020 Antero Nousiainen

#include <player/MouseReleasedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::player
{
namespace
{
TEST(MouseReleasedEventTest, SubscribeMouseReleasedEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        MouseReleasedEvent::subscribe([&subscriberInvoked](int, int) {
            subscriberInvoked = true;
        });

    std::invoke(MouseReleasedEvent{}, 42, 313);
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::player
