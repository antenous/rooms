// Copyright (c) 2022 Antero Nousiainen

#include <player/ComputerPlayer.hpp>
#include <gameboard/Gameboard.hpp>
#include <gtest/gtest.h>

namespace rooms::player
{
namespace
{
using namespace std::chrono_literals;
using namespace testing;

class TestGame
{
public:
    TestGame(int size, const gameboard::GameboardView::Walls& walls) :
        gameboard{{1, 1, size}}
    {
        for (auto wall : walls)
            gameboard.build(wall);
    }

    void build(const WallPosition& wall)
    {
        gameboard.build(wall);
    }

private:
    gameboard::Gameboard gameboard;
};

class ComputerPlayerTest : public Test
{
protected:
    void playTurn(TestGame& game)
    {
        auto turn = ComputerPlayer::Turn{};
        auto move = turn.get_future();
        player.play(std::move(turn));

        ASSERT_EQ(std::future_status::ready, move.wait_for(0s));
        game.build(move.get());
    }

    int roomsCompletable(TestGame& game)
    {
        auto roomsCompletable = 0;

        for (; !gameboard.lastWalls().empty(); ++roomsCompletable)
            playTurn(game);

        return roomsCompletable;
    }

    ComputerPlayer player{core::PlayerId::p1, "computer"};
    gameboard::GameboardView gameboard;
};

TEST_F(ComputerPlayerTest, IsPlayer)
{
    EXPECT_TRUE((std::is_base_of_v<core::Player, ComputerPlayer>));
}

TEST_F(ComputerPlayerTest, CompleteRoomsMissingLastWall)
{
    /*
     +---+---+---+
     |   |   :   |
     +...+.......+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    auto game = TestGame{3, {{{0, 0}, Side::right}}};
    ASSERT_FALSE(gameboard.lastWalls().empty());

    playTurn(game);
    EXPECT_TRUE(gameboard.lastWalls().empty());
}

TEST_F(ComputerPlayerTest, BuildWallsToRoomsWithOneOrTwoWalls)
{
    /*
     +---+---+---+
     |   :   :   |
     +...+.......+
     |   |   :   |
     +...+.......+
     |   :   :   |
     +---+---+---+
     */
    auto game = TestGame{3, {{{1, 1}, Side::left}}};
    ASSERT_FALSE(gameboard.safeWalls().empty());
    EXPECT_TRUE(gameboard.lastWalls().empty());

    playTurn(game);
    EXPECT_TRUE(gameboard.safeWalls().empty());
    EXPECT_TRUE(gameboard.lastWalls().empty());
}

TEST_F(ComputerPlayerTest, BuildWallsThatLeadToLeastCompletableRooms)
{
    /*
     +---+---+---+
     |   :   :   |
     +...+...+...+
     |   |   |   |
     +...+...+...+
     |   :   :   |
     +---+---+---+
     */
    auto game = TestGame{3, {{{1, 1}, Side::left}, {{1, 1}, Side::right}}};
    ASSERT_TRUE(gameboard.lastWalls().empty());

    playTurn(game);
    EXPECT_FALSE(gameboard.lastWalls().empty());
    EXPECT_EQ(1, roomsCompletable(game));
}
} // namespace
} // namespace rooms::player
