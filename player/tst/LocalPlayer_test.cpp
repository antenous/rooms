// Copyright (c) 2021 Antero Nousiainen

#include <player/LocalPlayer.hpp>
#include <player/MouseMovedEvent.hpp>
#include <player/MouseReleasedEvent.hpp>
#include <common/Utility.hpp>
#include <gameboard/Gameboard.hpp>
#include <fmt/format.h>
#include <gmock/gmock.h>

namespace rooms
{
inline namespace
{
// LCOV_EXCL_START
void PrintTo(const WallPosition& wall, std::ostream* os)
{
    *os << fmt::format(
        "{{({}, {}), {}}}", wall.position.x(), wall.position.y(), to_underlying(wall.side));
}
// LCOV_EXCL_STOP
} // namespace
} // namespace rooms

namespace rooms::player
{
namespace
{
using namespace std::chrono_literals;
using namespace testing;

class MockGame
{
public:
    Dimensions dimensions{5, 5, 3};
    gameboard::Gameboard gameboard{dimensions};
};

class LocalPlayerTest : public Test
{
protected:
    void buildWall(Point position, Side side)
    {
        game.gameboard.build({position, side});
    }

    auto giveTurn()
    {
        auto turn = LocalPlayer::Turn{};
        auto move = turn.get_future();
        player.play(std::move(turn));
        return move;
    }

    void moveMouseTo(Point point)
    {
        const auto [x, y] = point;
        mouseMoved(x * game.dimensions.width + 1, y * game.dimensions.height + 1);
    }

    void clickMouse()
    {
        mouseReleased(0, 0);
    }

    LocalPlayer player{core::PlayerId::p1, "local"};
    MockGame game;
    MouseMovedEvent mouseMoved{};
    MouseReleasedEvent mouseReleased{};
};

TEST_F(LocalPlayerTest, IsPlayer)
{
    EXPECT_TRUE((std::is_base_of_v<core::Player, LocalPlayer>));
}

TEST_F(LocalPlayerTest, DoNothingWhenNoTurnIsGiven)
{
    EXPECT_NO_THROW(clickMouse()) << "the mouse click event should not have been subscribed";
}

TEST_F(LocalPlayerTest, DoNothingWhenNoWallIsSelected)
{
    const auto move = giveTurn();

    clickMouse();

    EXPECT_NE(std::future_status::ready, move.wait_for(0s));
    EXPECT_TRUE(move.valid());
}

TEST_F(LocalPlayerTest, DoNothingWhenNoWallIsSelectable)
{
    /*
     +---+---+---+
     |   |   :   |
     +---+.......+
     |   :   :   |
     +...........+
     |   :   :   |
     +---+---+---+
     */
    const auto room = Point{0, 0};
    buildWall(room, Side::right);
    buildWall(room, Side::bottom);

    auto captured = std::optional<WallPosition>{};
    signals::ScopedConnection subscription =
        WallSelectedEvent::subscribe([&captured](const WallPosition& wall) {
            captured = wall; // LCOV_EXCL_LINE
        });

    const auto move = giveTurn();
    moveMouseTo(room);
    EXPECT_FALSE(captured);

    clickMouse();
    EXPECT_NE(std::future_status::ready, move.wait_for(0s));
    EXPECT_TRUE(move.valid());
}

TEST_F(LocalPlayerTest, SelectNearestUnbuiltWallOnMouseMove)
{
    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...+...+...+
     |   |   |   |
     +---+---+---+
     */
    const auto room = Point{1, 2};
    buildWall(room, Side::right);
    buildWall(room, Side::left);

    auto captured = std::optional<WallPosition>{};
    signals::ScopedConnection subscription =
        WallSelectedEvent::subscribe([&captured](const WallPosition& wall) {
            captured = wall;
        });

    const auto move = giveTurn();
    moveMouseTo(room);
    EXPECT_EQ((WallPosition{room, Side::top}), captured);
    EXPECT_NE(std::future_status::ready, move.wait_for(0s));
    EXPECT_TRUE(move.valid());
}

TEST_F(LocalPlayerTest, BuildSelectedWallOnMouseReleased)
{
    /*
     +---+---+---+
     |   :   :   |
     +...........+
     |   :   :   |
     +...+---+...+
     |   :   |   |
     +---+---+---+
     */
    const auto room = Point{1, 2};
    buildWall(room, Side::top);
    buildWall(room, Side::right);

    auto move = giveTurn();
    moveMouseTo(room);

    clickMouse();
    ASSERT_EQ(std::future_status::ready, move.wait_for(0s));
    EXPECT_EQ((WallPosition{room, Side::left}), move.get());
}

TEST_F(LocalPlayerTest, BuildSelectedWallOnlyOnce)
{
    const auto room = Point{1, 1};
    auto move = giveTurn();
    moveMouseTo(room);

    clickMouse();
    ASSERT_EQ(std::future_status::ready, move.wait_for(0s));
    move.get();

    EXPECT_NO_THROW(clickMouse()) << "the mouse click event should no longer be subscribed";
}

TEST_F(LocalPlayerTest, BuildOnlyOneWallWhenGivenTurn)
{
    const auto room = Point{1, 1};
    auto move = giveTurn();

    moveMouseTo(room);
    clickMouse();

    ASSERT_EQ(std::future_status::ready, move.wait_for(0s));
    move.get();

    moveMouseTo(room);
    EXPECT_NO_THROW(clickMouse()) << "the mouse click event should no longer be subscribed";
}
} // namespace
} // namespace rooms::player
