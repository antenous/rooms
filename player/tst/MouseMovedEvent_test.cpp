// Copyright (c) 2020 Antero Nousiainen

#include <player/MouseMovedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::player
{
namespace
{
TEST(MouseMovedEventTest, SubscribeMouseMovedEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        MouseMovedEvent::subscribe([&subscriberInvoked](int, int) {
            subscriberInvoked = true;
        });

    std::invoke(MouseMovedEvent{}, 42, 313);
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::player
