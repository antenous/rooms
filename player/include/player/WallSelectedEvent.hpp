// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_PLAYER_WALLSELECTEDEVENT_HPP_
#define ROOMS_PLAYER_WALLSELECTEDEVENT_HPP_

#include <common/WallPosition.hpp>
#include <signals/Event.hpp>

namespace rooms::player
{

struct WallSelectedEvent : signals::Event<WallSelectedEvent, void(const WallPosition&)>
{
};

} // namespace rooms::player

#endif
