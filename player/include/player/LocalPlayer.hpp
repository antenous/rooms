// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_PLAYER_LOCALPLAYER_HPP_
#define ROOMS_PLAYER_LOCALPLAYER_HPP_

#include "WallSelectedEvent.hpp"
#include <core/Player.hpp>
#include <gameboard/GameboardView.hpp>
#include <signals/ScopedConnection.hpp>

namespace rooms::player
{

class LocalPlayer : public core::Player
{
public:
    using Player::Player;

    void play(Turn&& move) override;

private:
    Turn myTurn;
    gameboard::GameboardView gameboard;
    signals::ScopedConnection mouseMovedSubscription;
    signals::ScopedConnection mouseReleasedSubscription;
    WallSelectedEvent wallSelected{};
};

} // namespace rooms::player

#endif
