// Copyright (c) 2020 Antero Nousiainen

#ifndef ROOMS_PLAYER_MOUSEMOVEDEVENT_HPP_
#define ROOMS_PLAYER_MOUSEMOVEDEVENT_HPP_

#include <signals/Event.hpp>

namespace rooms::player
{

struct MouseMovedEvent : public signals::Event<MouseMovedEvent, void(int, int)>
{
};

} // namespace rooms::player

#endif
