// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_PLAYER_COMPUTERPLAYER_HPP_
#define ROOMS_PLAYER_COMPUTERPLAYER_HPP_

#include <core/Player.hpp>
#include <gameboard/GameboardView.hpp>

namespace rooms::player
{

class ComputerPlayer : public core::Player
{
public:
    using Player::Player;

    void play(Turn&& turn) override;

private:
    using Walls = gameboard::GameboardView::Walls;

    void randomize(Turn& turn, const Walls& walls);

    gameboard::GameboardView gameboard;
};

} // namespace rooms::player

#endif
