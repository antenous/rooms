// Copyright (c) 2020 Antero Nousiainen

#ifndef ROOMS_PLAYER_MOUSERELEASEDEVENT_HPP_
#define ROOMS_PLAYER_MOUSERELEASEDEVENT_HPP_

#include <signals/Event.hpp>

namespace rooms::player
{

struct MouseReleasedEvent : public signals::Event<MouseReleasedEvent, void(int, int)>
{
};

} // namespace rooms::player

#endif
