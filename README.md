# rooms

A game of rooms

[![pipeline status](https://gitlab.com/antenous/rooms/badges/master/pipeline.svg)](https://gitlab.com/antenous/rooms/-/commits/master)
[![coverage report](https://gitlab.com/antenous/rooms/badges/master/coverage.svg)](https://gitlab.com/antenous/rooms/-/commits/master)
[![Latest Release](https://gitlab.com/antenous/rooms/-/badges/release.svg)](https://gitlab.com/antenous/rooms/-/releases)

The object of the game is to conquer the gameboard by building walls until all four
walls of the room are complete. The game is won by the player who has built most of
the rooms (i.e. built the last missing walls).

## History

This project was born when a simple ["Hello, World!"][1] program was no longer a
sufficient platform for developing programming skills and experimenting new things.
And it was fun to see how a simple pen-and-paper game played during recess would
work on a computer.

The project was forgotten for several years until it got a fresh start as an
exercise in refactoring and revamping legacy code that had scattered logic, no unit
tests, and was platform dependent (to name a few).

[1]: https://en.wikipedia.org/wiki/%22Hello,_World!%22_program

## Getting started

The following tools are needed to develop the project:
* [CMake](https://cmake.org/) version 3.21 or later
* [gcc](https://gcc.gnu.org/) version 10 or later or
  [Clang](https://clang.llvm.org/) version 16 or later
* [git](https://git-scm.com/)
* [Make](https://www.gnu.org/software/make/) or
  [Ninja](https://ninja-build.org/)
* [Qt](https://www.qt.io/) version 6 or later
* [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) version 14 or later

Tools used to measure code coverage:
* [lcov](http://ltp.sourceforge.net/coverage/lcov.php)
* [gcovr](https://gcovr.com)

Other tools used in this project (fetched at configure time):
* [signals](https://gitlab.com/antenous/signals)
* [{fmt}](https://github.com/fmtlib/fmt)
* [GoogleTest](https://github.com/google/googletest)

## Building

These instructions assume that the project has been cloned into a directory
named `rooms`. To configure and build a project, run the commands below. 

```sh
$ cd rooms
$ cmake -S . -B build/
$ cmake --build build/
```

This will create an executable named `rooms` in the `build/src/` folder.

To reformat the code after making changes, run, for example,
the `git clang-format` [command][2].

[2]: https://clang.llvm.org/docs/ClangFormat.html#git-integration

Both `GNU` and `Clang` support colored diagnostics but depending on the build
environment the colored output may not be enabled by default. To force colored
diagnostics configure the project with `COLOR_DIAGNOSTICS=On`.

```sh
$ cmake -DCOLOR_DIAGNOSTICS=On build/
```

### Building with Docker

Setting up a development environment is sometimes a challenge; the build
tools and libraries may differ between systems, even the underlying OS may
not be compatible. To separate the build process from the build system, a
[Docker](https://www.docker.com/) development environment is provided. With it,
the build environment is always the same regardless of the surrounding system.

Some IDEs (e.g. [Eclipse](https://www.eclipse.org/)) support building inside a
Docker container. Run the command below to create the image.

```sh
$ docker build \
  --build-arg USER=`id -un` \
  --build-arg UID=`id -u` \
  --build-arg GID=`id -g` \
  --build-arg TZ=`cat /etc/timezone` \
  -t rooms:dev .
```

Start the development container by running the command below.

```sh
$ docker run --rm -it \
  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
  -v $PWD:$PWD -v $(ccache -k cache_dir):$HOME/.ccache/ \
  -w $PWD -h rooms-docker --name rooms rooms:dev
```

> **NOTE!** The Docker commands above may require root permissions

> **NOTE!** Depdending on the host build environment, CMake may need to be
reconfigured in the running container

### Building with MSVC and Ninja on Windows

Install [Ninja](https://ninja-build.org/) (and [ccache](https://ccache.dev/)) in
`/usr/bin/` (or `%userprofile%/bin/`). Prepare the build environment by running
the `vcvarsall.bat` script.

> **NOTE!** In [Git BASH](https://gitforwindows.org/), the environment variables
set by the batch script are not propagated to the running shell. To work around
this, run the command below.

```sh
$ ProgramFiles='Program Files (x86)'; \
  batch=$(find "/c/${ProgramFiles}" -name vcvarsall.bat -printf "C:/${ProgramFiles}/%P\n" -quit 2>/dev/null); \
  eval "$(MSYS_NO_PATHCONV=1 cmd "/C "${batch}" amd64 > nul && bash -c 'export -p'")"
```

Run the commands below to configure and build the project
(assuming, for example, that Qt 6.4.0 is installed in `C:/Qt`).

```sh
$ cmake -S . -B build/msvc/ -G Ninja -DCMAKE_PREFIX_PATH="C:/Qt/6.4.0/msvc2019_64/"
$ cmake --build build/msvc/
```

## Testing

To run unit tests, build the `check` target.

```sh
$ cmake --build build/ --target check
```

This builds and runs unit tests and shows a summary of the results.
A more detailed message is shown if any of the tests fail.

### Code coverage

To measure code coverage, configure the project with
`CODE_COVERAGE=On` and then build the `code-coverage` target.

```sh
$ cmake -DCODE_COVERAGE=On build/
$ cmake --build build/ --target check-coverage
```

This generates a code coverage report in the `build/rooms-coverage`
folder. View `build/rooms-coverage/index.html` to see the results.

> **NOTE!** Enabling code coverage forces the build type to be `Debug`

## Packaging

CMake comes with [CPack](https://cmake.org/cmake/help/latest/module/CPack.html),
a cross-platform software packaging tool that makes it easy to create distributable
packages. To create a binary package, run the command below.

```sh
$ cmake --build build/ --target package
```

To build a source package, run the command below.

```sh
$ cmake --build build/ --target package_source
```

## License

rooms is distributed under the MIT
[license](https://gitlab.com/antenous/rooms/-/blob/master/LICENSE).
