# To use the Docker development environment, see README.md


FROM ubuntu:22.04
ARG USER="dev"
ARG GROUP=${USER}
ARG UID=1000
ARG GID=${UID}
ARG TZ="Europe/Helsinki"
RUN groupadd -rg ${GID} ${GROUP} && \
    useradd --no-log-init -mrg ${GROUP} -u ${UID} ${USER}
RUN echo ${TZ} > /etc/timezone && \
    apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends \
    bash-completion build-essential ca-certificates ccache clang-format cmake \
    gcovr git lcov libgl1-mesa-dev ninja-build qt6-base-dev && \
    rm -rf /var/lib/apt/lists/* && \
    update-ccache-symlinks
RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends \
    gnupg lsb-release software-properties-common wget && \
    wget https://apt.llvm.org/llvm.sh && \
    chmod +x llvm.sh && \
    ./llvm.sh 16 && \
    rm -rf /var/lib/apt/lists/*
USER ${USER}
VOLUME /home/${USER}/.ccache/
WORKDIR /home/${USER}/
