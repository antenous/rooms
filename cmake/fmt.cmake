cmake_minimum_required(VERSION 3.14)
include(FetchContent)

# Mark {fmt}'s headers as system headers
# https://github.com/fmtlib/fmt/issues/2644
set(FMT_SYSTEM_HEADERS ON)

# NOTE: The above is not needed with CMake 3.25 and SYSTEM argument
# https://cmake.org/cmake/help/v3.25/module/FetchContent.html?highlight=fetchcontent#command:fetchcontent_declare

FetchContent_Declare(fmt
    GIT_REPOSITORY https://github.com/fmtlib/fmt.git
    GIT_TAG        9.1.0)

FetchContent_MakeAvailable(fmt)
