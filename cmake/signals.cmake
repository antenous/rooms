cmake_minimum_required(VERSION 3.14)
include(FetchContent)

FetchContent_Declare(signals
    GIT_REPOSITORY https://gitlab.com/antenous/signals.git
    GIT_TAG        6e050d8d97190815d7c112c1979141c28c0554b2)

FetchContent_MakeAvailable(signals)
