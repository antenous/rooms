cmake_minimum_required(VERSION 3.10)
include_guard(GLOBAL)

# Enable ccache if not already enabled by symlink masquerading and if no other
# CMake compiler launchers are already defined
find_program(CCACHE ccache)
mark_as_advanced(CCACHE)
if(CCACHE)
    if(NOT DEFINED CMAKE_CXX_COMPILER_LAUNCHER AND NOT CMAKE_CXX_COMPILER MATCHES "/ccache/")
      message(STATUS "Enabling ccache")
      set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE} CACHE STRING "")
    endif()
endif()
