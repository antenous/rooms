set(lib "core")
add_library(${lib} STATIC
    src/Game.cpp
    src/GameLoader.cpp
    src/GameRecorder.cpp
    src/GameSave.cpp
    src/Player.cpp
    src/QuickGame.cpp)
target_compile_options(${lib} PRIVATE
    $<$<OR:$<CXX_COMPILER_ID:GNU>,$<CXX_COMPILER_ID:Clang>>:
        -Wall -Werror -Wextra -pedantic>
    $<$<CXX_COMPILER_ID:MSVC>:/W4 /WX>)
target_include_directories(${lib} PUBLIC include)
target_link_libraries(${lib} PUBLIC
    gameboard fmt::fmt signals::signals)

add_coverage(${lib})
add_subdirectory(tst EXCLUDE_FROM_ALL)
