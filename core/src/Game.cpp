// Copyright (c) 2005 Antero Nousiainen

#include "core/Game.hpp"
#include "GameRecorder.hpp"
#include "core/Context.hpp"
#include "core/RoomCompletedEvent.hpp"
#include <gameboard/Gameboard.hpp>

namespace rooms::core
{

class Game::GameImpl : public std::enable_shared_from_this<GameImpl>
{
public:
    GameImpl(Context& context, const PlayerFactory& factory, const Dimensions& dimensions);

    void start();

    void generate(const WallPosition& wall);

    void replay(const WallPosition& wall, PlayerId id);

    void save(std::ostream& file);

private:
    void asyncWait();

    int buildWall(const WallPosition& wall, PlayerId id);

    void changeTurn();

    bool gameOver();

    bool isCurrentPlayerAboutToLose() const;

    bool isNext(PlayerId id) const;

    void nextTurn();

    void play();

    void processFrame(const WallPosition& wall);

    void processInput(const WallPosition& wall);

    void scoreTurn(int points);

    void poll();

    Context& context;
    PlayerFactory::Players players;
    PlayerFactory::Players::size_type current = 0;
    PlayerFactory::Players::size_type next = 1;
    bool started = false;
    gameboard::Gameboard gameboard;
    GameRecorder gameRecorder;
    std::future<WallPosition> move;

    GameStartedEvent gameStarted{};
    GameEndedEvent gameEnded{};
    RoomCompletedEvent roomCompleted{};
};

Game::WrongTurn::WrongTurn() :
    CoreError("wrong turn")
{
}

Game::CompletedByNonPlayer::CompletedByNonPlayer() :
    CoreError("room completed by a non-player")
{
}

Game::Game(Context& context, const PlayerFactory& factory, const Dimensions& dimensions) :
    pimpl(std::make_shared<GameImpl>(context, factory, dimensions))
{
}

Game::GameImpl::GameImpl(
    Context& context, const PlayerFactory& factory, const Dimensions& dimensions) :
    context(context),
    players(factory.createPlayers()),
    gameboard(dimensions),
    gameRecorder(dimensions)
{
}

void Game::start()
{
    pimpl->start();
}

void Game::GameImpl::start()
{
    if (gameOver())
        return;

    gameStarted();
    nextTurn();
}

bool Game::GameImpl::gameOver()
{
    if (!gameboard.gameOver())
        return false;

    if (isCurrentPlayerAboutToLose())
        changeTurn();

    gameEnded({players[current]->score(), players[next]->score()});
    return true;
}

bool Game::GameImpl::isCurrentPlayerAboutToLose() const
{
    return *players[current] < *players[next];
}

void Game::GameImpl::nextTurn()
{
    // Adding a short delay between turns creates a nice-looking animation
    context.defer([weak = weak_from_this()] {
        if (auto game = weak.lock(); game)
            game->play();
    });
}

void Game::GameImpl::play()
{
    auto turn = Player::Turn{};
    move = turn.get_future();
    players[current]->play(std::move(turn));
    asyncWait();
}

void Game::GameImpl::asyncWait()
{
    // TODO: With C++23 and std::move_only_function, "move" could be attached to the wait handler
    context.post([weak = weak_from_this()] {
        if (auto game = weak.lock(); game)
            game->poll();
    });
}

void Game::GameImpl::poll()
{
    using namespace std::chrono_literals;

    if (move.wait_for(5ms) != std::future_status::ready)
        return asyncWait();

    processFrame(move.get());
}

void Game::GameImpl::processFrame(const WallPosition& wall)
{
    processInput(wall);

    if (gameOver())
        return;

    nextTurn();
}

void Game::GameImpl::processInput(const WallPosition& wall)
{
    if (const auto roomsCompleted = buildWall(wall, players[current]->id()); roomsCompleted)
        scoreTurn(roomsCompleted);
    else
        changeTurn();
}

int Game::GameImpl::buildWall(const WallPosition& wall, PlayerId id)
{
    const auto roomsCompleted = gameboard.build(wall);
    gameRecorder.record(wall, id);

    for (auto room : roomsCompleted)
        roomCompleted(room, id);

    return std::ssize(roomsCompleted);
}

bool Game::GameImpl::isNext(PlayerId id) const
{
    return id == players[current]->id();
}

void Game::GameImpl::scoreTurn(int points)
{
    players[current]->add(points);
}

void Game::GameImpl::changeTurn()
{
    std::swap(current, next);
}

void Game::generate(const WallPosition& wall)
{
    pimpl->generate(wall);
}

void Game::GameImpl::generate(const WallPosition& wall)
{
    if (started)
        throw WrongTurn{};

    if (buildWall(wall, PlayerId::none) != 0)
        throw CompletedByNonPlayer{};
}

void Game::replay(const WallPosition& wall, PlayerId id)
{
    pimpl->replay(wall, id);
}

void Game::GameImpl::replay(const WallPosition& wall, PlayerId id)
{
    if (!isNext(id))
        throw WrongTurn{};

    processInput(wall);
    started = true;
}

void Game::save(std::ostream& file)
{
    pimpl->save(file);
}

void Game::GameImpl::save(std::ostream& file)
{
    gameRecorder.save(file);
}

} // namespace rooms::core
