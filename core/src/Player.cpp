// Copyright (c) 2005 Antero Nousiainen

#include "core/Player.hpp"

namespace rooms::core
{
Player::Player(PlayerId id, std::string name) :
    scorecard{std::move(name), id, 0}
{
}

auto Player::operator<=>(const Player& rhs) const -> std::weak_ordering
{
    return scorecard.points <=> rhs.scorecard.points;
}

auto Player::id() const -> PlayerId
{
    return scorecard.id;
}

void Player::add(int points)
{
    scorecard.points += points;
}

auto Player::score() const -> Scorecard
{
    return scorecard;
}
} // namespace rooms::core
