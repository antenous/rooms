// Copyright (c) 2024 Antero Nousiainen

#include "core/GameSave.hpp"
#include <common/Utility.hpp>
#include <fmt/format.h>

namespace fmt
{
template<>
struct formatter<rooms::core::GameSave::Move>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin())
    {
        return ctx.begin();
    }

    template<typename FormatContext>
    auto format(const rooms::core::GameSave::Move& move, FormatContext& ctx) const
        -> decltype(ctx.out())
    {
        return format_to(
            ctx.out(), "{} {} {} {}", rooms::to_underlying(move.id), move.wall.position.x(),
            move.wall.position.y(), rooms::to_underlying(move.wall.side));
    }
};
} // namespace fmt

namespace rooms::core
{
namespace
{
std::istream& operator>>(std::istream& is, PlayerId& id)
{
    return is >> reinterpret_cast<std::underlying_type_t<PlayerId>&>(id);
}

std::istream& operator>>(std::istream& is, Point& position)
{
    return is >> position.rx() >> position.ry();
}

std::istream& operator>>(std::istream& is, Side& side)
{
    return is >> reinterpret_cast<std::underlying_type_t<Side>&>(side);
}

std::istream& operator>>(std::istream& is, WallPosition& wall)
{
    return is >> wall.position >> wall.side;
}

std::istream& operator>>(std::istream& is, GameSave::Move& event)
{
    return is >> event.id >> event.wall;
}
} // namespace

std::ostream& operator<<(std::ostream& os, const GameSave& save)
{
    os << fmt::format(
        "{}\n{}\n{}\n", save.size, save.moves.size(), fmt::join(save.moves, "\n"));

    return os;
}

std::istream& operator>>(std::istream& is, GameSave& save)
{
    auto movesCount = GameSave::Moves::size_type{0};
    is >> save.size >> movesCount;
    save.moves.resize(movesCount);

    for (auto& move : save.moves)
        is >> move;

    return is;
}

} // namespace rooms::core
