// Copyright (c) 2020 Antero Nousiainen

#include "GameRecorder.hpp"
#include "core/CoreError.hpp"

namespace rooms::core
{
namespace
{
inline bool isPlayerMove(PlayerId id)
{
    return (id != PlayerId::none);
}
} // namespace

GameRecorder::GameRecorder(const Dimensions& dimensions) :
    gameSave{.moves = {}, .size = dimensions.size}
{
}

void GameRecorder::record(const WallPosition& wall, PlayerId id)
{
    hasPlayerMoves |= isPlayerMove(id);
    gameSave.moves.emplace_back(wall, id);
}

void GameRecorder::save(std::ostream& file)
{
    if (!hasPlayerMoves)
        throw CoreError("no player moves");

    file << gameSave;
}
} // namespace rooms::core
