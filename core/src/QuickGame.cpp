// Copyright (c) 2021 Antero Nousiainen

#include "core/QuickGame.hpp"
#include "core/Initializable.hpp"
#include <common/Random.hpp>

namespace rooms::core
{
void QuickGame::setUp(Initializable& game, int wallsLeftUnbuilt)
{
    // Because the gameboard view lists most walls twice (front and back),
    // the threshold for unbuilt walls needs to be doubled.
    for (auto walls = gameboard.safeWalls(); wallsLeftUnbuilt * 2 < std::ssize(walls);
         walls = gameboard.safeWalls())
        randomize(game, walls);
}

void QuickGame::randomize(Initializable& game, const Walls& walls)
{
    game.generate(walls[random(walls.size() - 1)]);
}
} // namespace rooms::core
