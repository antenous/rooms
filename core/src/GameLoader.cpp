// Copyright (c) 2024 Antero Nousiainen

#include "core/GameLoader.hpp"
#include "core/Initializable.hpp"
#include <gameboard/GameboardError.hpp>

namespace rooms::core
{

GameLoader::LoadError::LoadError() :
    CoreError("load error")
{
}

GameLoader::RestoreError::RestoreError() :
    CoreError("restore error")
{
}

void GameLoader::load(std::istream& file)
{
    try
    {
        file.exceptions(std::istream::failbit);
        file >> gameSave;
    }
    catch (const std::istream::failure&)
    {
        throw LoadError{};
    }
}

int GameLoader::size() const
{
    return gameSave.size;
}

void GameLoader::restore(Initializable& game) const
{
    try
    {
        for (auto [wall, id] : gameSave.moves)
            if (id != PlayerId::none)
                game.replay(wall, id);
            else
                game.generate(wall);
    }
    catch (const CoreError&)
    {
        throw RestoreError{};
    }
    catch (const gameboard::GameboardError&)
    {
        throw RestoreError{};
    }
}

} // namespace rooms::core
