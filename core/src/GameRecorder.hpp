// Copyright (c) 2020 Antero Nousiainen

#ifndef ROOMS_CORE_GAMERECORDER_HPP_
#define ROOMS_CORE_GAMERECORDER_HPP_

#include "core/GameSave.hpp"
#include <common/Dimensions.hpp>
#include <ostream>

namespace rooms::core
{

class GameRecorder
{
public:
    explicit GameRecorder(const Dimensions& dimensions);

    void record(const WallPosition& wall, PlayerId id);

    void save(std::ostream& file);

private:
    GameSave gameSave;
    bool hasPlayerMoves = false;
};

} // namespace rooms::core

#endif
