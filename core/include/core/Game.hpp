// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_CORE_GAME_HPP_
#define ROOMS_CORE_GAME_HPP_

#include "CoreError.hpp"
#include "GameEndedEvent.hpp"
#include "GameStartedEvent.hpp"
#include "Initializable.hpp"
#include "PlayerFactory.hpp"
#include <common/Dimensions.hpp>
#include <ostream>

namespace rooms::core
{

class Context;

class Game : public Initializable
{
public:
    class CompletedByNonPlayer;

    class WrongTurn;

    Game(Context& context, const PlayerFactory& factory, const Dimensions& dimensions);

    Game(const Game&) = delete;

    Game(Game&&) = default;

    ~Game() = default;

    Game& operator=(const Game&) = delete;

    Game& operator=(Game&&) = default;

    void generate(const WallPosition& wall) override;

    void replay(const WallPosition& wall, PlayerId id) override;

    void start();

    void save(std::ostream& file);

private:
    class GameImpl;

    std::shared_ptr<GameImpl> pimpl;
};

class Game::CompletedByNonPlayer : public CoreError
{
public:
    CompletedByNonPlayer();
};

class Game::WrongTurn : public CoreError
{
public:
    WrongTurn();
};

} // namespace rooms::core

#endif
