// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_CORE_GAMESTARTEDEVENT_HPP_
#define ROOMS_CORE_GAMESTARTEDEVENT_HPP_

#include <signals/Event.hpp>

namespace rooms::core
{

struct GameStartedEvent : signals::Event<GameStartedEvent, void()>
{
};

} // namespace rooms::core

#endif
