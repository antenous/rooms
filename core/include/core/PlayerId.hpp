// Copyright (c) 2024 Antero Nousiainen

#ifndef ROOMS_CORE_PLAYERID_HPP_
#define ROOMS_CORE_PLAYERID_HPP_

namespace rooms::core
{

enum class PlayerId
{
    p1,
    p2,
    none
};

} // namespace rooms::core

#endif
