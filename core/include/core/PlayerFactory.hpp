// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_CORE_PLAYERFACTORY_HPP_
#define ROOMS_CORE_PLAYERFACTORY_HPP_

#include "Player.hpp"
#include <array>
#include <memory>

namespace rooms::core
{

class PlayerFactory
{
public:
    using Players = std::array<std::unique_ptr<Player>, 2>;

    virtual ~PlayerFactory() = default;

    virtual Players createPlayers() const = 0;
};

} // namespace rooms::core

#endif
