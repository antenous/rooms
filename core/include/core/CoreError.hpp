// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_CORE_COREERROR_HPP_
#define ROOMS_CORE_COREERROR_HPP_

#include <stdexcept>

namespace rooms::core
{

class CoreError : public std::runtime_error
{
public:
    using std::runtime_error::runtime_error;
};

} // namespace rooms::core

#endif
