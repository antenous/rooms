// Copyright (c) 2024 Antero Nousiainen

#ifndef ROOMS_CORE_GAMELOADER_HPP_
#define ROOMS_CORE_GAMELOADER_HPP_

#include "CoreError.hpp"
#include "GameSave.hpp"
#include <istream>

namespace rooms::core
{

class Initializable;

class GameLoader
{
public:
    class LoadError;

    class RestoreError;

    void load(std::istream& file);

    void restore(Initializable& game) const;

    [[nodiscard]] int size() const;

private:
    GameSave gameSave;
};

class GameLoader::LoadError : public CoreError
{
public:
    LoadError();
};

class GameLoader::RestoreError : public CoreError
{
public:
    RestoreError();
};

} // namespace rooms::core

#endif
