// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_CORE_ROOMCOMPLETEDEVENT_HPP_
#define ROOMS_CORE_ROOMCOMPLETEDEVENT_HPP_

#include "PlayerId.hpp"
#include <common/Point.hpp>
#include <signals/Event.hpp>

namespace rooms::core
{

struct RoomCompletedEvent : signals::Event<RoomCompletedEvent, void(Point, PlayerId)>
{
};

} // namespace rooms::core

#endif
