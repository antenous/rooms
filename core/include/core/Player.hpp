// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_CORE_PLAYER_HPP_
#define ROOMS_CORE_PLAYER_HPP_

#include "PlayerId.hpp"
#include "Scorecard.hpp"
#include <common/WallPosition.hpp>
#include <compare>
#include <future>
#include <string>

namespace rooms::core
{

class Player
{
public:
    using Turn = std::promise<WallPosition>;

    Player(PlayerId id, std::string name);

    virtual ~Player() = default;

    auto operator<=>(const Player& rhs) const -> std::weak_ordering;

    void add(int points);

    auto id() const -> PlayerId;

    virtual void play(Turn&& turn) = 0;

    auto score() const -> Scorecard;

private:
    Scorecard scorecard = {};
};

} // namespace rooms::core

#endif
