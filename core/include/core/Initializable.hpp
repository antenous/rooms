// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_CORE_INITIALIZABLE_HPP_
#define ROOMS_CORE_INITIALIZABLE_HPP_

#include "PlayerId.hpp"
#include <common/WallPosition.hpp>

namespace rooms::core
{

class Initializable
{
public:
    virtual ~Initializable() = default;

    /**
     Generate a wall on the gameboard.

     @note
     Wall generation is not allowed after a player has built the first
     wall. Wall generation is also not allowed to make complete rooms.
     */
    virtual void generate(const WallPosition& wall) = 0;

    /**
     Replay a move.

     @note
     A move is a wall built by a player, walls built
     using the "generate" method above are not moves.
     */
    virtual void replay(const WallPosition& wall, PlayerId id) = 0;
};

} // namespace rooms::core

#endif
