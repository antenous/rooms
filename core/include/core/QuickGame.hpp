// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_CORE_QUICKGAME_HPP_
#define ROOMS_CORE_QUICKGAME_HPP_

#include <gameboard/GameboardView.hpp>

namespace rooms::core
{

class Initializable;

/**
 Quick game is a game mode where the gameboard is pre-filled with
 randomly built walls so that the rooms have a maximum of two walls.

 @note
 Because the gameboard view is based on room and wall events, the quick game
 must be instantiated before the gameboard for it to function properly.
 */
class QuickGame
{
public:
    void setUp(Initializable& game, int wallsLeftUnbuilt = 0);

private:
    using Walls = gameboard::GameboardView::Walls;

    void randomize(Initializable& game, const Walls& walls);

    gameboard::GameboardView gameboard;
};

} // namespace rooms::core

#endif
