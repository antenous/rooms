// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_CORE_GAMEENDEDEVENT_HPP_
#define ROOMS_CORE_GAMEENDEDEVENT_HPP_

#include "Scorecard.hpp"
#include <signals/Event.hpp>
#include <array>

namespace rooms::core
{

using Scorecards = std::array<Scorecard, 2>;

struct GameEndedEvent : signals::Event<GameEndedEvent, void(const Scorecards&)>
{
};

} // namespace rooms::core

#endif
