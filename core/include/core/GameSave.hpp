// Copyright (c) 2024 Antero Nousiainen

#ifndef ROOMS_CORE_GAMESAVE_HPP_
#define ROOMS_CORE_GAMESAVE_HPP_

#include "PlayerId.hpp"
#include <common/WallPosition.hpp>
#include <iostream>
#include <vector>

namespace rooms::core
{

struct GameSave
{
    // TODO: Another "Move" is already defined in Player
    struct Move
    {
        constexpr bool operator==(const Move&) const = default;

        WallPosition wall;
        PlayerId id;
    };

    using Moves = std::vector<Move>;

    Moves moves;
    int size;
};

std::ostream& operator<<(std::ostream& os, const GameSave& save);

std::istream& operator>>(std::istream& is, GameSave& save);

} // namespace rooms::core

#endif
