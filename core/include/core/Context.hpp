// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_CORE_CONTEXT_HPP_
#define ROOMS_CORE_CONTEXT_HPP_

#include <functional>

namespace rooms::core
{

class Context
{
public:
    using Callable = std::function<void()>;

    virtual ~Context() = default;

    /**
     Schedule the callable for deferred execution
     */
    virtual void defer(Callable callable) = 0;

    /**
     Schedule the callable for immediate execution
     */
    virtual void post(Callable callable) = 0;
};

} // namespace rooms::core

#endif
