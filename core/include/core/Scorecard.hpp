// Copyright (c) 2024 Antero Nousiainen

#ifndef ROOMS_CORE_SCORECARD_HPP_
#define ROOMS_CORE_SCORECARD_HPP_

#include "PlayerId.hpp"
#include <string>

namespace rooms::core
{

struct Scorecard
{
    std::string name;
    PlayerId id;
    int points = 0;
};

} // namespace rooms::core

#endif
