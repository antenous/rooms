// Copyright (c) 2021 Antero Nousiainen

#include <core/GameEndedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(GameEndedEventTest, SubscribeGameEndedEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        GameEndedEvent::subscribe([&subscriberInvoked](const Scorecards&) {
            subscriberInvoked = true;
        });

    std::invoke(GameEndedEvent{}, Scorecards{});
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::core
