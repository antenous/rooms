// Copyright (c) 2022 Antero Nousiainen

#include <core/Player.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
class DummyPlayer : public Player
{
public:
    DummyPlayer() :
        Player(PlayerId::p1, "dummy")
    {
    }

    void play(Turn&&) override // LCOV_EXCL_LINE
    {
    }
};

TEST(PlayerTest, HasVirtualDestructor)
{
    EXPECT_TRUE(std::has_virtual_destructor_v<Player>);
}

TEST(PlayerTest, IsAbstract)
{
    EXPECT_TRUE(std::is_abstract_v<Player>);
}

TEST(PlayerTest, Scorecard)
{
    auto p1 = DummyPlayer{};
    auto score = p1.score();

    EXPECT_EQ(PlayerId::p1, score.id);
    EXPECT_EQ("dummy", score.name);
    EXPECT_EQ(0, score.points);

    p1.add(42);

    EXPECT_EQ(42, p1.score().points);
}

TEST(PlayerTest, PlayerWithFewerRoomsComparesLess)
{
    auto p1 = DummyPlayer{};
    auto p2 = DummyPlayer{};

    EXPECT_FALSE(p1 < p2);

    p2.add(1);

    EXPECT_TRUE(p1 < p2);
}
} // namespace
} // namespace rooms::core
