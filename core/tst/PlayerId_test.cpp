// Copyright (c) 2024 Antero Nousiainen

#include <core/PlayerId.hpp>
#include <common/Utility.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(PlayerIdTest, Values)
{
    // To protect against unintentional changes in value
    // because the enum is used in the save file.
    EXPECT_EQ(0, to_underlying(PlayerId::p1));
    EXPECT_EQ(1, to_underlying(PlayerId::p2));
    EXPECT_EQ(2, to_underlying(PlayerId::none));
}
} // namespace
} // namespace rooms::core
