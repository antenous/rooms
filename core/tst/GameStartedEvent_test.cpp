// Copyright (c) 2022 Antero Nousiainen

#include <core/GameStartedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(GameStartedEventTest, SubscribeGameStartedEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        GameStartedEvent::subscribe([&subscriberInvoked] {
            subscriberInvoked = true;
        });

    std::invoke(GameStartedEvent{});
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::core
