// Copyright (c) 2024 Antero Nousiainen

#include <core/GameLoader.hpp>
#include <core/Initializable.hpp>
#include <gameboard/GameboardError.hpp>
#include <gmock/gmock.h>

namespace rooms::core
{
namespace
{

using namespace testing;

class MockGame : public Initializable
{
public:
    MOCK_METHOD(void, generate, (const WallPosition&), (override));

    MOCK_METHOD(void, replay, (const WallPosition&, PlayerId), (override));
};

class GameLoaderTest : public Test
{
protected:
    GameLoader loader;
    MockGame game;
};

TEST_F(GameLoaderTest, CanThrowAndCatchLoadError)
{
    const auto throwLoadError = [] {
        throw GameLoader::LoadError{};
    };
    EXPECT_THAT(throwLoadError, ThrowsMessage<CoreError>(StrEq("load error")));
}

TEST_F(GameLoaderTest, ThrowLoadErrorWhenGamesaveIsBad)
{
    auto badGamesave = std::istringstream{};
    badGamesave.setstate(std::ios::badbit);
    EXPECT_THROW(loader.load(badGamesave), GameLoader::LoadError);
}

TEST_F(GameLoaderTest, ThrowLoadErrorWhenGamesaveIsEmpty)
{
    auto emptyGamesave = std::istringstream{};
    EXPECT_THROW(loader.load(emptyGamesave), GameLoader::LoadError);
}

TEST_F(GameLoaderTest, ReadGameboardSizeWhenGameIsFirstSavedAndThenLoaded)
{
    auto file = std::stringstream{};
    file << GameSave{{}, 2};

    loader.load(file);
    EXPECT_EQ(2, loader.size());
}

TEST_F(GameLoaderTest, CanThrowAndCatchReplayError)
{
    const auto throwReplayError = [] {
        throw GameLoader::RestoreError{};
    };
    EXPECT_THAT(throwReplayError, ThrowsMessage<CoreError>(StrEq("restore error")));
}

TEST_F(GameLoaderTest, ThrowReplayErrorWhenReplayFailsDueToCoreError)
{
    auto file = std::stringstream{};
    file << GameSave{{{}}, 42};

    loader.load(file);

    EXPECT_CALL(game, replay).WillOnce(Throw(CoreError{"what"}));
    EXPECT_THROW(loader.restore(game), GameLoader::RestoreError);
}

TEST_F(GameLoaderTest, ThrowReplayErrorWhenReplayFailsDueToGameboardError)
{
    auto file = std::stringstream{};
    file << GameSave{{{}}, 42};

    loader.load(file);

    EXPECT_CALL(game, replay).WillOnce(Throw(gameboard::GameboardError{"what"}));
    EXPECT_THROW(loader.restore(game), GameLoader::RestoreError);
}

TEST_F(GameLoaderTest, CanReplayGameWhenSaveIsFirstLoaded)
{
    auto save = GameSave{};
    save.moves.emplace_back(WallPosition{{0, 0}, Side::right}, PlayerId::none);
    save.moves.emplace_back(WallPosition{{1, 1}, Side::top}, PlayerId::p1);

    auto file = std::stringstream{};
    file << save;

    loader.load(file);

    EXPECT_CALL(game, generate(WallPosition{{0, 0}, Side::right}));
    EXPECT_CALL(game, replay(WallPosition{{1, 1}, Side::top}, PlayerId::p1));
    loader.restore(game);
}

} // namespace
} // namespace rooms::core
