// Copyright (c) 2021 Antero Nousiainen

#include <core/Game.hpp>
#include <core/Context.hpp>
#include <core/GameSave.hpp>
#include <core/RoomCompletedEvent.hpp>
#include <common/Utility.hpp>
#include <gameboard/Gameboard.hpp>
#include <signals/ScopedConnection.hpp>
#include <fmt/format.h>
#include <gmock/gmock.h>
#include <queue>

namespace rooms::core
{
namespace
{
using namespace testing;

struct Event
{
    int id;
    Point room;
    int wall;
};

class FakeContext : public Context
{
public:
    void defer(Callable callable) override
    {
        queue.push(std::move(callable));
    }

    void post(Callable callable) override
    {
        queue.push(std::move(callable));
    }

    int runOne()
    {
        if (stopped())
            return 0;

        std::invoke(queue.front());
        queue.pop();
        return 1;
    }

    bool stopped() const
    {
        return queue.empty();
    }

private:
    std::queue<Callable> queue;
};

class GameOverSpy
{
public:
    GameOverSpy()
    {
        subscription = GameEndedEvent::subscribe([this](const Scorecards& scorecards) {
            name = scorecards.front().name;
        });
    }

    std::string winner() const
    {
        return name;
    }

private:
    std::string name;
    signals::ScopedConnection subscription;
};

class TestPlayer : public Player
{
public:
    TestPlayer(PlayerId id) :
        Player(id, id == PlayerId::p1 ? "p1" : "p2")
    {
    }

    bool assignedTurn() const
    {
        return myTurn.has_value();
    }

    void playTurn(const WallPosition& wall)
    {
        auto turn = std::exchange(myTurn, std::nullopt);
        turn->set_value(wall);
    }

private:
    void play(Turn&& turn) override
    {
        myTurn = std::move(turn);
    }

    std::optional<Turn> myTurn;
};

class TestPlayerFactory : public PlayerFactory
{
public:
    TestPlayerFactory(TestPlayer*& p1, TestPlayer*& p2) :
        p1(p1),
        p2(p2)
    {
    }

    Players createPlayers() const override
    {
        auto players = PlayerFactory::Players{
            std::make_unique<TestPlayer>(PlayerId::p1),
            std::make_unique<TestPlayer>(PlayerId::p2)};

        p1 = dynamic_cast<TestPlayer*>(players.front().get());
        p2 = dynamic_cast<TestPlayer*>(players.back().get());

        return players;
    }

private:
    TestPlayer*& p1;
    TestPlayer*& p2;
};

class GameTest : public Test
{
protected:
    using Recordings = std::vector<Event>;

    void SetUp() override
    {
        game = createGame(4);
    }

    auto createGame(int size) -> std::shared_ptr<Game>
    {
        return std::make_shared<Game>(
            context, TestPlayerFactory{p1, p2}, Dimensions{width, height, size});
    }

    void play(TestPlayer* player, const WallPosition& wall)
    {
        context.runOne();
        ASSERT_TRUE(player->assignedTurn());
        player->playTurn(wall);
        context.runOne();
    }

    auto replay(const Recordings& recordings)
    {
        GameOverSpy gameOver;

        for (auto event : recordings)
            if (auto id = static_cast<PlayerId>(event.id); id != PlayerId::none)
                game->replay({event.room, static_cast<Side>(event.wall)}, id);
            else
                game->generate({event.room, static_cast<Side>(event.wall)});

        game->start();
        return gameOver.winner();
    }

    std::shared_ptr<Game> game;

    const int width = 5;
    const int height = 5;

    FakeContext context;
    TestPlayer* p1 = nullptr;
    TestPlayer* p2 = nullptr;
};

TEST_F(GameTest, IsNonCopyable)
{
    // Game is non-copyable because there is no point in copying it (and since pimpl is
    // implemented using std::shared_ptr instead of std::unique_ptr, a copy of the Game
    // would not be a full copy (because the pimpl would be shared))
    EXPECT_FALSE(std::is_copy_constructible_v<Game>);
    EXPECT_FALSE(std::is_copy_assignable_v<Game>);
}

TEST_F(GameTest, IsMovable)
{
    EXPECT_TRUE(std::is_move_constructible_v<Game>);
    EXPECT_TRUE(std::is_move_assignable_v<Game>);
}

TEST_F(GameTest, IsInitializable)
{
    EXPECT_TRUE((std::is_base_of_v<Initializable, Game>));
}

TEST_F(GameTest, CreateGameboardWhenConstructed)
{
    auto roomsCreated = 0;

    signals::ScopedConnection scopedSubscription =
        gameboard::RoomCreatedEvent::subscribe([&roomsCreated](auto) {
            ++roomsCreated;
        });

    const auto size = 42;
    createGame(size);
    EXPECT_EQ(size * size, roomsCreated);
}

TEST_F(GameTest, CreatePlayersWhenConstructed)
{
    EXPECT_NE(nullptr, p1);
    EXPECT_NE(nullptr, p2);
}

TEST_F(GameTest, PlayersAreNotAssignedTurnsByDefault)
{
    EXPECT_FALSE(p1->assignedTurn());
    EXPECT_FALSE(p2->assignedTurn());
}

TEST_F(GameTest, ThrowWhenReplayingAndPositionIsOffGameboard)
{
    EXPECT_THROW((replay({{0, {0, 42}, 2}})), gameboard::Gameboard::PositionOffGameboard);
    EXPECT_THROW((replay({{0, {42, 0}, 2}})), gameboard::Gameboard::PositionOffGameboard);
}

TEST_F(GameTest, CanThrowAndCatchWrongTurn)
{
    const auto throwWrongTurn = [] {
        throw Game::WrongTurn{};
    };
    EXPECT_THAT(throwWrongTurn, ThrowsMessage<CoreError>(StrEq("wrong turn")));
}

TEST_F(GameTest, ThrowWhenReplayingAndPlayerPlaysWrongTurn)
{
    EXPECT_THROW((replay({{1, {0, 0}, 2}})), Game::WrongTurn);
}

TEST_F(GameTest, ThrowWhenReplayingAndNonPlayerPlaysWrongTurn)
{
    // A non-player is allowed to play only until the first player plays a turn

    replay({{0, {1, 1}, 1}});

    EXPECT_THROW((replay({{2, {1, 1}, 2}})), Game::WrongTurn);
}

TEST_F(GameTest, ThrowWhenReplayingAndNonPlayerCompletesRoom)
{
    // In a normal game, only the player on turn can build walls, but when setting up
    // a quick game or loading a previous game, it is possible for a non-player to create
    // walls. However, the non-player is not allowed to complete the rooms (i.e. build
    // the last missing walls).
    const auto badReplay = [this] {
        replay({{2, {0, 0}, 2}, {2, {0, 0}, 3}});
    };

    EXPECT_THAT(badReplay, ThrowsMessage<CoreError>(StrEq("room completed by a non-player")));
}

TEST_F(GameTest, DoNotStartGameWhenGameIsOver)
{
    signals::ScopedConnection scopedSubscription = GameStartedEvent::subscribe([] {
        FAIL() << "unexpected event"; // LCOV_EXCL_LINE
    });

    // A game with only one room is over by default
    createGame(1)->start();
    EXPECT_EQ(0, context.runOne());
    EXPECT_TRUE(context.stopped());
}

TEST_F(GameTest, NotifyListenersWhenGameIsStarted)
{
    auto gameStarted = false;
    signals::ScopedConnection scopedSubscription =
        GameStartedEvent::subscribe([&gameStarted]() {
            gameStarted = true;
        });

    game->start();
    EXPECT_TRUE(gameStarted);
}

TEST_F(GameTest, PlayerOneIsAssignedTurnWhenGameIsStarted)
{
    game->start();

    ASSERT_FALSE(context.stopped());
    context.runOne();

    EXPECT_TRUE(p1->assignedTurn());
    EXPECT_FALSE(p2->assignedTurn());
}

TEST_F(GameTest, AsyncPlayIsLifetimeManaged)
{
    game->start();
    game.reset();

    // Without lifetime management, playing a turn
    // after the game is reset results in segfault
    context.runOne();
}

TEST_F(GameTest, DoNotHangWhenPlayerIsNotImmediatelyReadyToBuildWall)
{
    // TODO: This test is a bit too tightly coupled to the implementation

    game = createGame(2);
    game->start();

    EXPECT_EQ(1, context.runOne());
    ASSERT_TRUE(p1->assignedTurn());

    // Simulate the passage of time. If the result of std::futures is waited unconditionally,
    // this test will hang.
    EXPECT_EQ(1, context.runOne());

    p1->playTurn({{0, 0}, Side::bottom});
    EXPECT_EQ(1, context.runOne());

    EXPECT_EQ(1, context.runOne());
    EXPECT_TRUE(p2->assignedTurn());
}

TEST_F(GameTest, NotifyListenersWhenRoomIsCompleted)
{
    auto events = std::vector<std::string>{};
    signals::ScopedConnection scopedSubscription =
        RoomCompletedEvent::subscribe([&events](Point position, PlayerId id) {
            events.push_back(fmt::format(
                "({}, {}):p{}", position.x(), position.y(), to_underlying(id) + 1));
        });

    game->start();

    /* 1st wall:
     +---+---+---+---+
     |   :   :   :   |
     +---+...+...+...+
     |   :   :   :   |
     +...+...+...+...|
     |   :   :   :   |
     */
    play(p1, {{0, 0}, Side::bottom});
    EXPECT_THAT(events, IsEmpty());

    /* 2nd wall:
     +---+---+---+---+
     |   |   :   :   |
     +---+---+...+...+
     |   :   :   :   |
     +...+...+...+...|
     |   :   :   :   |
     */
    play(p2, {{0, 0}, Side::right});
    ASSERT_THAT(events, SizeIs(1));
    EXPECT_EQ("(0, 0):p2", events[0]);

    /* 3rd and 4th walls:
     +---+---+---+---+
     |   |   |   :   |
     +---+---+...+...+
     |   :   :   :   |
     +...+...+...+...|
     |   :   :   :   |
     */
    play(p2, {{1, 1}, Side::top});
    play(p1, {{2, 0}, Side::left});
    ASSERT_THAT(events, SizeIs(2));
    EXPECT_EQ("(1, 0):p1", events[1]);

    /* 5th, 6th and 7th walls:
     +---+---+---+---+
     |   |   |   :   |
     +---+---+...+...+
     |   :   |   :   |
     +---+---+...+...|
     |   :   :   :   |
     */
    play(p1, {{2, 1}, Side::left});
    play(p2, {{1, 1}, Side::bottom});
    play(p1, {{0, 2}, Side::top});
    ASSERT_THAT(events, SizeIs(2));

    /* 8th wall
     +---+---+---+---+
     |   |   |   :   |
     +---+---+...+...+
     |   |   |   :   |
     +---+---+...+...|
     |   :   :   :   |
     */
    play(p2, {{1, 1}, Side::left});
    ASSERT_THAT(events, SizeIs(4));
    EXPECT_EQ("(0, 1):p2", events[2]);
    EXPECT_EQ("(1, 1):p2", events[3]);
}

TEST_F(GameTest, PlayGame)
{
    GameOverSpy gameOver;

    /*
     +---+---+
     |   :   |
     +...+...+
     |   :   |
     +---+---+
     */
    game = createGame(2);
    game->start();

    /* 1st wall:
     +---+---+
     |   :   |
     +---+...+
     |   :   |
     +---+---+
     */
    play(p1, {{0, 0}, Side::bottom});

    /* 2nd wall:
     +---+---+
     |   |   |
     +---+...+
     |   :   |
     +---+---+
     */
    play(p2, {{0, 0}, Side::right});

    /* 3rd wall, player 2 continues:
     +---+---+
     |   |   |
     +---+---+
     |   :   |
     +---+---+
     */
    play(p2, {{1, 1}, Side::top});

    /* 4th wall, player 2 wins
     +---+---+
     |   |   |
     +---+---+
     |   |   |
     +---+---+
     */
    play(p2, {{1, 1}, Side::left});
    EXPECT_EQ(0, p1->score().points);
    EXPECT_EQ(4, p2->score().points);

    // No more turns to play
    ASSERT_TRUE(context.stopped());
    ASSERT_FALSE(p1->assignedTurn());
    ASSERT_FALSE(p2->assignedTurn());

    EXPECT_EQ(gameOver.winner(), p2->score().name);
}

TEST_F(GameTest, SaveGame)
{
    GameOverSpy gameOver;

    game = createGame(2);
    game->start();

    // 1st wall:
    play(p1, {{0, 0}, Side::bottom});

    // 2nd wall:
    play(p2, {{0, 0}, Side::right});

    // 3rd wall, player 2 continues:
    play(p2, {{1, 1}, Side::top});

    auto file = std::stringstream{};
    game->save(file);

    auto save = GameSave{};
    file >> save;

    EXPECT_EQ(2, save.size);
    ASSERT_EQ(3, std::ssize(save.moves));
    EXPECT_EQ((GameSave::Move{{{0, 0}, Side::bottom}, PlayerId::p1}), save.moves[0]);
    EXPECT_EQ((GameSave::Move{{{0, 0}, Side::right}, PlayerId::p2}), save.moves[1]);
    EXPECT_EQ((GameSave::Move{{{1, 1}, Side::top}, PlayerId::p2}), save.moves[2]);
}

TEST_F(GameTest, SaveGameWhenReplayedGameContainsPlayerMoves)
{
    game = createGame(3);
    game->generate({{0, 0}, Side::right});
    game->replay({{0, 1}, Side::right}, PlayerId::p1);
    game->replay({{0, 2}, Side::right}, PlayerId::p2);

    auto file = std::stringstream{};
    game->save(file);

    auto save = GameSave{};
    file >> save;

    EXPECT_EQ(3, save.size);
    ASSERT_EQ(3, std::ssize(save.moves));
    EXPECT_EQ((GameSave::Move{{{0, 0}, Side::right}, PlayerId::none}), save.moves[0]);
    EXPECT_EQ((GameSave::Move{{{0, 1}, Side::right}, PlayerId::p1}), save.moves[1]);
    EXPECT_EQ((GameSave::Move{{{0, 2}, Side::right}, PlayerId::p2}), save.moves[2]);
}

TEST_F(GameTest, ThrowWhenReplayingMoveByNonPlayer)
{
    EXPECT_THROW(game->replay({{0, 0}, Side::right}, PlayerId::none), Game::WrongTurn);
}

TEST_F(GameTest, DoNotRecordMoveWhenItThrows)
{
    GameOverSpy gameOver;

    game = createGame(7);
    game->start();

    // 1st wall:
    play(p1, {{0, 0}, Side::right});

    // 2nd wall:
    EXPECT_THROW((play(p2, {{0, 0}, Side::right})), gameboard::GameboardError);

    auto file = std::stringstream{};
    game->save(file);

    auto save = GameSave{};
    file >> save;

    EXPECT_EQ(7, save.size);
    ASSERT_EQ(1, std::ssize(save.moves));
    EXPECT_EQ((GameSave::Move{{{0, 0}, Side::right}, PlayerId::p1}), save.moves[0]);
}

TEST_F(GameTest, ThrowWhenSavingGameThatHasNoPlayerMoves)
{
    game = createGame(7);
    game->generate({{0, 0}, Side::right});
    game->start();

    auto file = std::stringstream{};
    EXPECT_THROW(game->save(file), CoreError);

    // Just to make sure the save file is empty
    EXPECT_THAT(file.str(), IsEmpty());
}

TEST_F(GameTest, PlayerOneAlwaysStartsAfterQuickGame)
{
    // Quick game creates walls with the "none" player,
    // and this should not affect player turns.
    game->generate({{0, 0}, Side::right});
    game->start();
    ASSERT_FALSE(context.stopped());

    context.runOne();
    EXPECT_TRUE(p1->assignedTurn());
    EXPECT_FALSE(p2->assignedTurn());
}

TEST_F(GameTest, DoNotAssignTurnsToPlayersWhenGameIsReplayed)
{
    game->replay({{0, 0}, Side::right}, p1->id());
    EXPECT_TRUE(context.stopped());
    EXPECT_FALSE(p1->assignedTurn());
    EXPECT_FALSE(p2->assignedTurn());

    game->replay({{0, 0}, Side::bottom}, p2->id());
    EXPECT_TRUE(context.stopped());
    EXPECT_FALSE(p1->assignedTurn());
    EXPECT_FALSE(p2->assignedTurn());
}

TEST_F(GameTest, EndGameOnlyAfterItHasBeenStarted)
{
    signals::ScopedConnection subscription = GameEndedEvent::subscribe([](auto&) {
        FAIL() << "unexpected event"; // LCOV_EXCL_LINE
    });

    game = createGame(2);
    game->replay({{0, 0}, Side::right}, p1->id());
    game->replay({{1, 1}, Side::left}, p2->id());
    game->replay({{0, 0}, Side::bottom}, p1->id());
    game->replay({{1, 1}, Side::top}, p1->id());

    auto gameOver = false;
    subscription = GameEndedEvent::subscribe([&gameOver](auto&) {
        gameOver = true;
    });

    game->start();
    EXPECT_TRUE(gameOver);
}

TEST_F(GameTest, PlayerOneWinsWhenRoomCountIsSameButPlayerOneBuiltLastWall)
{
    // clang-format off
    const Recordings recordings{
        {2, {0, 1}, 2},
        {2, {2, 2}, 3},
        {2, {2, 1}, 2},
        {2, {1, 1}, 2},
        {2, {1, 0}, 2},
        {2, {1, 3}, 3},
        {0, {2, 0}, 2},
        {1, {2, 3}, 3},
        {0, {2, 2}, 2},
        {0, {3, 2}, 2},
        {0, {3, 1}, 2},
        {0, {1, 2}, 3},
        {0, {2, 1}, 3},
        {1, {1, 1}, 3},
        {1, {3, 0}, 2},
        {1, {0, 1}, 3},
        {1, {2, 0}, 3},
        {1, {0, 0}, 2},
        {1, {1, 0}, 3},
        {1, {0, 0}, 3},
        {1, {0, 2}, 3},
        {0, {0, 2}, 2},
        {0, {0, 3}, 3},
        {0, {1, 2}, 2}};
    // clang-format on

    EXPECT_EQ(replay(recordings), p1->score().name);
    EXPECT_TRUE(context.stopped());
}

TEST_F(GameTest, PlayerTwoWinsWhenRoomCountIsSameButPlayerTwoBuiltLastWall)
{
    // clang-format off
    const Recordings recordings{
        {2, {1, 1}, 2},
        {2, {3, 1}, 2},
        {2, {1, 3}, 3},
        {2, {0, 1}, 2},
        {2, {2, 1}, 2},
        {2, {1, 1}, 3},
        {0, {1, 2}, 3},
        {1, {2, 3}, 3},
        {0, {2, 2}, 2},
        {0, {2, 2}, 3},
        {0, {3, 2}, 2},
        {0, {1, 0}, 3},
        {1, {0, 2}, 2},
        {0, {0, 3}, 3},
        {0, {1, 2}, 2},
        {0, {0, 2}, 3},
        {0, {0, 1}, 3},
        {1, {0, 0}, 2},
        {1, {1, 0}, 2},
        {1, {0, 0}, 3},
        {1, {2, 0}, 3},
        {0, {2, 1}, 3},
        {1, {2, 0}, 2},
        {1, {3, 0}, 2}};
    // clang-format on
    EXPECT_EQ(replay(recordings), p2->score().name);
    EXPECT_TRUE(context.stopped());
}

TEST_F(GameTest, PlayerTwoWinsWhenItHasGreaterRoomCountWhilePlayerOneBuiltLastWall)
{
    // clang-format off
    const Recordings recordings{
        {2, {0, 1}, 2},
        {2, {2, 2}, 3},
        {2, {2, 1}, 2},
        {2, {1, 1}, 2},
        {2, {1, 0}, 2},
        {2, {1, 3}, 3},
        {0, {2, 0}, 2},
        {1, {2, 2}, 2},
        {0, {3, 0}, 2},
        {1, {1, 2}, 3},
        {1, {2, 3}, 3},
        {1, {2, 0}, 3},
        {1, {3, 2}, 2},
        {1, {1, 0}, 3},
        {1, {3, 1}, 2},
        {1, {0, 0}, 3},
        {1, {2, 1}, 3},
        {1, {0, 0}, 2},
        {1, {1, 1}, 3},
        {1, {0, 1}, 3},
        {1, {0, 3}, 3},
        {0, {0, 2}, 2},
        {0, {0, 2}, 3},
        {0, {1, 2}, 2}};
    // clang-format on
    EXPECT_EQ(replay(recordings), p2->score().name);
    EXPECT_TRUE(context.stopped());
}
} // namespace
} // namespace rooms::core
