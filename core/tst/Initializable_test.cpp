// Copyright (c) 2021 Antero Nousiainen

#include <core/Initializable.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(InitializableTest, HasVirtualDestructor)
{
    EXPECT_TRUE(std::has_virtual_destructor_v<Initializable>);
}

TEST(InitializableTest, IsAbstract)
{
    EXPECT_TRUE(std::is_abstract_v<Initializable>);
}
} // namespace
} // namespace rooms::core
