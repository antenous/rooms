// Copyright (c) 2021 Antero Nousiainen

#include <core/PlayerFactory.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(PlayerFactoryTest, HasVirtualDestructor)
{
    EXPECT_TRUE(std::has_virtual_destructor_v<PlayerFactory>);
}

TEST(PlayerFactoryTest, IsAbstract)
{
    EXPECT_TRUE(std::is_abstract_v<PlayerFactory>);
}
} // namespace
} // namespace rooms::core
