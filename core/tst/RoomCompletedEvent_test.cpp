// Copyright (c) 2022 Antero Nousiainen

#include <core/RoomCompletedEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(RoomCompletedEventTest, SubscribeEvent)
{
    auto subscriberInvoked = false;

    signals::ScopedConnection scopedSubscription =
        RoomCompletedEvent::subscribe([&subscriberInvoked](Point, PlayerId) {
            subscriberInvoked = true;
        });

    std::invoke(RoomCompletedEvent{}, Point{}, PlayerId::none);
    EXPECT_TRUE(subscriberInvoked);
}
} // namespace
} // namespace rooms::core
