// Copyright (c) 2021 Antero Nousiainen

#include <core/Context.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{
TEST(ContextTest, HasVirtualDestructor)
{
    EXPECT_TRUE(std::has_virtual_destructor_v<Context>);
}

TEST(ContextTest, IsAbstract)
{
    EXPECT_TRUE(std::is_abstract_v<Context>);
}
} // namespace
} // namespace rooms::core
