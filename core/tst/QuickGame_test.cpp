// Copyright (c) 2021 Antero Nousiainen

#include <core/QuickGame.hpp>
#include <core/Initializable.hpp>
#include <gameboard/Gameboard.hpp>
#include <gameboard/WallBuiltEvent.hpp>
#include <signals/ScopedConnection.hpp>
#include <gmock/gmock.h>

namespace rooms::core
{
namespace
{
using namespace gameboard;
using namespace testing;

class Game : public Initializable
{
public:
    explicit Game(int size) :
        gameboard{{1, 1, size}}
    {
    }

    void generate(const WallPosition& wall) override
    {
        gameboard.build(wall);
    }

    void replay(const WallPosition&, PlayerId) override // LCOV_EXCL_LINE
    {
        throw std::runtime_error("unexpected function call"); // LCOV_EXCL_LINE
    }

private:
    Gameboard gameboard;
};

class QuickGameTest : public Test
{
public:
    MOCK_METHOD(void, onWallBuilt, (const WallPosition&), ());

protected:
    void SetUp() override
    {
        wallBuiltSubscription = WallBuiltEvent::subscribe([this](const WallPosition& wall) {
            onWallBuilt(wall);
        });
    }

    QuickGame quickGame;
    signals::ScopedConnection wallBuiltSubscription;
};

TEST_F(QuickGameTest, DoNothingWhenNoRoomHasOneOrZeroWalls)
{
    // On a 2x2 gameboard, all rooms have 2 walls.
    Game game{2};
    EXPECT_CALL(*this, onWallBuilt).Times(0);
    quickGame.setUp(game);
}

TEST_F(QuickGameTest, BuildRandomizedWalls)
{
    // A 3x3 gameboard has 4 rooms with one wall and one without walls
    // and a total of 4 walls, 2 of which can be built.
    Game game{3};
    EXPECT_CALL(*this, onWallBuilt).Times(2);
    quickGame.setUp(game);
}

TEST_F(QuickGameTest, StopBuildingWallsWhenThresholdForUnbuiltWallsIsReached)
{
    // Because a 3x3 gameboard has four walls, of which only two can be built,
    // the threshold for unbuilt walls must be greater than 2 for it to have an effect.
    Game game{3};
    EXPECT_CALL(*this, onWallBuilt).Times(1);
    quickGame.setUp(game, 3);
}

TEST_F(QuickGameTest, WallsAreNotRebuilt)
{
    // Because the walls are built randomly, a large gameboard is needed to
    // increase the chance of rebuilding a wall if the walls already built
    // are not removed.
    wallBuiltSubscription.disconnect();
    Game game{13};
    EXPECT_NO_THROW(quickGame.setUp(game));
}
} // namespace
} // namespace rooms::core
