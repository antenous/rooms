// Copyright (c) 2024 Antero Nousiainen

#include <core/GameSave.hpp>
#include <gtest/gtest.h>

namespace rooms::core
{
namespace
{

TEST(GameSaveTest, CanBeSerialized)
{
    auto save = GameSave{};
    save.size = 13;
    save.moves.emplace_back(WallPosition{{1, 2}, Side::right}, PlayerId::p1);
    save.moves.emplace_back(WallPosition{{0, 3}, Side::bottom}, PlayerId::p2);
    save.moves.emplace_back(WallPosition{{3, 0}, Side::left}, PlayerId::none);

    auto file = std::stringstream{};
    file << save;

    EXPECT_EQ(
        "13\n"
        "3\n"
        "0 1 2 3\n"
        "1 0 3 2\n"
        "2 3 0 1\n",
        file.str());
}

TEST(GameSaveTest, CanBeDeserialized)
{
    auto save = GameSave{};
    save.size = 13;
    save.moves.emplace_back(WallPosition{{1, 2}, Side::right}, PlayerId::p1);
    save.moves.emplace_back(WallPosition{{0, 3}, Side::bottom}, PlayerId::p2);
    save.moves.emplace_back(WallPosition{{3, 0}, Side::left}, PlayerId::none);

    auto file = std::stringstream{};
    file << save;

    auto loadedSave = GameSave{};
    file >> loadedSave;

    auto otherFile = std::ostringstream{};
    otherFile << loadedSave;

    EXPECT_EQ(file.str(), otherFile.str());
}

} // namespace
} // namespace rooms::core
