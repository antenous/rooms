// Copyright (c) 2022 Antero Nousiainen

#include <core/CoreError.hpp>
#include <gmock/gmock.h>

namespace rooms::core
{
namespace
{
using namespace testing;

TEST(CoreErrorTest, CanThrowAndCatchCoreError)
{
    const auto throwCoreError = [] {
        throw CoreError{"what"};
    };
    EXPECT_THAT(throwCoreError, ThrowsMessage<std::runtime_error>(StrEq("what")));
}
} // namespace
} // namespace rooms::core
