// Copyright (c) 2023 Antero Nousiainen

#include <common/Rect.hpp>
#include <common/Margins.hpp>
#include <gtest/gtest.h>

namespace rooms
{
namespace
{
TEST(RectTest, IsQRect)
{
    // To protect against unintentional type change
    EXPECT_TRUE((std::is_same_v<Rect, QRect>));
}

TEST(RectTest, ExtractSides)
{
    using enum Side;
    const Rect layout{0, 0, 7, 7};
    EXPECT_EQ((layout - Margins{0, 0, 0, layout.height()}), layout | extract(top));
    EXPECT_EQ((layout - Margins{0, 0, layout.width(), 0}), layout | extract(left));
    EXPECT_EQ((layout - Margins{0, layout.height(), 0, 0}), layout | extract(bottom));
    EXPECT_EQ((layout - Margins{layout.width(), 0, 0, 0}), layout | extract(right));
}
} // namespace
} // namespace rooms
