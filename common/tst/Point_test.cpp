// Copyright (c) 2022 Antero Nousiainen

#include <common/Point.hpp>
#include <gtest/gtest.h>

namespace rooms
{
namespace
{
TEST(PointTest, IsQPoint)
{
    // To protect against unintentional type change
    EXPECT_TRUE((std::is_same_v<Point, QPoint>));
}

class PointCmpTest : public testing::Test
{
protected:
    PointCmp cmp{};
};

TEST_F(PointCmpTest, PointWithLowerYValueComparesLess)
{
    EXPECT_TRUE(cmp({2, 0}, {2, 1}));
    EXPECT_TRUE(cmp({2, 0}, {3, 1}));
    EXPECT_TRUE(cmp({3, 0}, {2, 1}));

    EXPECT_FALSE(cmp({2, 1}, {2, 0}));
    EXPECT_FALSE(cmp({3, 1}, {2, 0}));
    EXPECT_FALSE(cmp({2, 1}, {3, 0}));
}

TEST_F(PointCmpTest, PointWithLowerXValueComparesLessWhenYValuesAreEqual)
{
    EXPECT_TRUE(cmp({0, 7}, {1, 7}));
    EXPECT_FALSE(cmp({1, 13}, {0, 13}));
}

TEST_F(PointCmpTest, EqualPointsDoNotCompareLess)
{
    EXPECT_FALSE(cmp({2, 3}, {2, 3}));
}
} // namespace
} // namespace rooms
