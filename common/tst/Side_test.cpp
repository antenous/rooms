// Copyright (c) 2022 Antero Nousiainen

#include <common/Side.hpp>
#include <common/Utility.hpp>
#include <gtest/gtest.h>

namespace rooms
{
namespace
{
TEST(SideTest, IsUsableForArrayIndexing)
{
    // To protect against unintentional changes in value
    // because the enum is used in array indexing.
    EXPECT_EQ(0, to_underlying(Side::top));
    EXPECT_EQ(1, to_underlying(Side::left));
    EXPECT_EQ(2, to_underlying(Side::bottom));
    EXPECT_EQ(3, to_underlying(Side::right));
}
} // namespace
} // namespace rooms
