// Copyright (c) 2023 Antero Nousiainen

#include <common/Pipeable.hpp>
#include <gtest/gtest.h>

namespace rooms
{
namespace
{
TEST(PipeableTest, Pipe)
{
    const auto squared = Pipeable{[](int i) {
        return i * i;
    }};
    EXPECT_EQ(9, 3 | squared);
}
} // namespace
} // namespace rooms
