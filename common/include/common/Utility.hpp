// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_COMMON_UTILITY_HPP_
#define ROOMS_COMMON_UTILITY_HPP_

#ifdef __cpp_lib_to_underlying
#include <utility>
namespace rooms
{
using to_underlying = std::to_underlying;
}
#else
// Workaround until C++23 is available
// https://en.cppreference.com/w/cpp/utility/to_underlying
#include <type_traits>
namespace rooms
{
template<class Enum>
constexpr std::underlying_type_t<Enum> to_underlying(Enum e) noexcept
{
    return static_cast<std::underlying_type_t<Enum>>(e);
}
} // namespace rooms
#endif // __cpp_lib_to_underlying

#ifdef __cpp_lib_unreachable
#include <utility>
namespace rooms
{
using unreachable = std::unreachable;
}
#else
// Workaround until C++23 is available
// https://en.cppreference.com/w/cpp/utility/unreachable
// LCOV_EXCL_START
namespace rooms
{
[[noreturn]] inline void unreachable()
{
#ifdef __GNUC__ // GCC, Clang, ICC
    __builtin_unreachable();
#elif defined _MSC_VER // MSVC
    __assume(false);
#endif // __GNUC__
}
} // namespace rooms
// LCOV_EXCL_STOP
#endif // __cpp_lib_unreachable

#endif
