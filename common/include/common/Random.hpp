// Copyright (c) 2023 Antero Nousiainen

#ifndef ROOMS_COMMON_RANDOM_HPP_
#define ROOMS_COMMON_RANDOM_HPP_

#include <random>

namespace rooms
{
namespace detail
{

using PRNG = std::mt19937;

auto prng() -> PRNG&;

} // namespace detail

template<typename IntType>
auto random(IntType min, IntType max) -> IntType
{
    return std::uniform_int_distribution<IntType>{min, max}(detail::prng());
}

template<typename IntType>
auto random(IntType max) -> IntType
{
    return random<IntType>(0, max);
}

} // namespace rooms

#endif
