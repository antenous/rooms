// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_COMMON_SIDE_HPP_
#define ROOMS_COMMON_SIDE_HPP_

namespace rooms
{

/**
 A side of a rectangle/tile
 */
enum class Side
{
    top,
    left,
    bottom,
    right
};

} // namespace rooms

#endif
