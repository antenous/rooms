// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_COMMON_WALLPOSITION_HPP_
#define ROOMS_COMMON_WALLPOSITION_HPP_

#include "Point.hpp"
#include "Side.hpp"

namespace rooms
{

struct WallPosition
{
    constexpr bool operator<(const WallPosition& rhs) const;

    constexpr bool operator==(const WallPosition& rhs) const = default;

    /**
     The (x,y) position of a room on the gameboard
     */
    Point position;

    /**
     A side of the room
     */
    Side side;
};

constexpr bool WallPosition::operator<(const WallPosition& rhs) const
{
    return position.y() < rhs.position.y() ||
        (position.y() == rhs.position.y() && position.x() < rhs.position.x()) ||
        (position == rhs.position && side < rhs.side);
}

} // namespace rooms

#endif
