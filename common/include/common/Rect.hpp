// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_COMMON_RECT_HPP_
#define ROOMS_COMMON_RECT_HPP_

#include "Pipeable.hpp"
#include "Side.hpp"
#include <QRect>

namespace rooms
{

using Rect = QRect;

/**
 A way to extract one side of a rectangle

 @code
 auto rect = Rect{0, 0, 7, 7};
 auto topSide = rect | extract(Side::top);
 @endcode
 */
struct SideExtractor
{
    auto operator()(const Rect& rect) const -> Rect;

    Side side;
};

auto extract(Side side) -> Pipeable<SideExtractor>;

} // namespace rooms

#endif
