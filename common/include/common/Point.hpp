// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_COMMON_POINT_HPP_
#define ROOMS_COMMON_POINT_HPP_

#include <QPoint>

namespace rooms
{

using Point = QPoint;

struct PointCmp
{
    constexpr bool operator()(const Point& lhs, const Point& rhs) const
    {
        return lhs.y() < rhs.y() || (lhs.y() == rhs.y() && lhs.x() < rhs.x());
    }
};

} // namespace rooms

#endif
