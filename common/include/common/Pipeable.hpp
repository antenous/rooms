// Copyright (c) 2023 Antero Nousiainen

#ifndef ROOMS_COMMON_PIPEABLE_HPP_
#define ROOMS_COMMON_PIPEABLE_HPP_

#include <functional>

namespace rooms
{

/**
 Helper for creating pipelines

 @code
 auto squared = Pipeable{[](int i){ return i*i; }};
 auto i = 3 | squared; // i = 9
 @endcode
 */
template<typename... Ts>
struct Pipeable : Ts...
{
    using Ts::operator()...;
};
template<typename... Ts>
Pipeable(Ts...) -> Pipeable<Ts...>;

template<typename U, typename... Ts>
decltype(auto) operator|(U&& u, const Pipeable<Ts...>& fn)
{
    return std::invoke(fn, std::forward<U>(u));
}

} // namespace rooms

#endif
