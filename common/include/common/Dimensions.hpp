// Copyright (c) 2022 Antero Nousiainen

#ifndef ROOMS_COMMON_DIMENSIONS_HPP_
#define ROOMS_COMMON_DIMENSIONS_HPP_

namespace rooms
{

struct Dimensions
{
    int width;
    int height;
    int size;
};

} // namespace rooms

#endif
