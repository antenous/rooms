// Copyright (c) 2021 Antero Nousiainen

#ifndef ROOMS_COMMON_MARGINS_HPP_
#define ROOMS_COMMON_MARGINS_HPP_

#include <QMargins>

namespace rooms
{

using Margins = QMargins;

}

#endif
