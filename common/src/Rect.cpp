// Copyright (c) 2023 Antero Nousiainen

#include "common/Rect.hpp"
#include "common/Margins.hpp"
#include "common/Utility.hpp"

namespace rooms
{

auto SideExtractor::operator()(const Rect& rect) const -> Rect
{
    switch (side)
    {
    case Side::top:
        return rect - Margins{0, 0, 0, rect.height()};

    case Side::left:
        return rect - Margins{0, 0, rect.width(), 0};

    case Side::bottom:
        return rect - Margins{0, rect.height(), 0, 0};

    case Side::right:
        return rect - Margins{rect.width(), 0, 0, 0};

    default: // LCOV_EXCL_LINE
        rooms::unreachable(); // LCOV_EXCL_LINE
    }
}

auto extract(Side side) -> Pipeable<SideExtractor>
{
    return {side};
}

} // namespace rooms
