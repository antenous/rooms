// Copyright (c) 2023 Antero Nousiainen

#include "common/Random.hpp"
#include <algorithm>
#include <array>
#include <functional>
#include <utility>

namespace rooms::detail
{

auto prng() -> PRNG&
{
    thread_local auto rd = std::random_device{};
    thread_local auto data = std::array<PRNG::result_type, PRNG::state_size>{};
    thread_local auto generated = false;

    if (!std::exchange(generated, true))
        std::ranges::generate(data, std::ref(rd));

    thread_local auto seeds = std::seed_seq(std::begin(data), std::end(data));
    thread_local auto engine = PRNG{seeds};
    return engine;
}

} // namespace rooms::detail
